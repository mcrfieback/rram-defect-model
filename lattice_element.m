classdef lattice_element
    properties
        % Position
        index = 0             % Index in the grid array
        x = 0                 % x position
        y = 0                 % y position
        z = 0                 % z position
        intAt = zeros(8,1)    % Interstitial atom at any of 8 bordering diagonals
        
        % Poisson
        A_row = zeros(0,2)    % (Sparsified) row vector in stiffness matrix
        Nabla_row = zeros(0,4)% (Sparsified) row vector in gradient matrix (for x,y,z)
        
        % Properties
        type = 'oxide'        % Type: oxide, lower, upper, OEL, OEL_ox, O-2, V+2
        isIL = false          % Lattice element is on the electrode-oxide interface layer
        E = 0                 % Trap energy
        R_T = 0               % Trap capture radius
        Q = 0                 % Element charge
        epsilon = 21          % Element relative dielectric permittivity
        
        % Field and potential
        phi = 0               % Local potential field
        phi_ex = 0            % External potential field (exc. Coulomb, for TATSolver)
        F_vec = [0; 0; 0]     % Local electric field
        F_vec_ex = [0; 0; 0]  % External electric field
        F = 0                 % Local electric field magnitude
        F_ex = 0              % External electric field magnitude (exc. Coulomb, for TATSolver)
        
        % Power dissipation
        T = 300               % Local temperature
        P = 0                 % Local power
        
        % Electrode locations
        d_U = 0               % Closest upper electrode distance
        d_L = 0               % Closest lower electrode distance
        index_U = 0           % Closest upper electrode index
        index_L = 0           % Closest lower electrode index
    end
    
    methods (Access=public)
        function o = lattice_element(varargin)
            % LATTICE_ELEMENT Accepts any number of arguments and sets the
            % properties of the element according to the order in which the
            % arguments are given (see lattice_element properties list)
            o_f = fields(o);
            for n = 1:length(varargin)
                o.(o_f{n}) = varargin{n};
            end
        end
        
        function typeNum = getTypeNumber(o)
            % GETTYPENUM Returns the type of the lattice element as a number.
            N = numel(o);
            typeNum = zeros(size(o));
            types = {'oxide', 'lower', 'upper', 'OEL', 'OEL_ox', 'O-2', 'V+2', 'air'};
            
            for n = 1:N; typeNum(n) = find(strcmp(types,o(n).type))-1; end
        end
        
        function check = is(o,type)
            if ischar(type) || isstring(type)
                check = strcmp({o.type},type);
                return;
            elseif iscell(type)
                check = false(size({o.type}));
                for n = 1:length(type)
                    check = check | strcmp({o.type},type{n});
                end
            end
        end
        
        function o = turnInto(o,type)
            % TURNINTO Returns the lattice element turned into another type.
            if ~any(strcmp(o.type,{'oxide', 'OEL_ox', 'O-2', 'V+2'}))
                error(['Can not change from ' o.type '.']);
            end
            if ~any(strcmp(type,{'oxide', 'OEL_ox', 'O-2', 'V+2'}))
                error(['Can not change into ' type '.']);
            end
            
            if strcmp(o.type,type)
                warning(['Point is already a ' type '.']);
                return;
            end
            
            global PROPS;
            q = 1.602e-19;
            
            o.type = type;
            if strcmp(type,'V+2')
                o.E = normrnd(PROPS.E_Tmu,PROPS.E_Tdelta)*q;
                o.R_T = PROPS.R_T;
%                 o.Q = 2*q;
            elseif strcmp(type,'O-2')
                o.E = 0;
                o.R_T = 0;
                o.Q = -q;
            elseif strcmp(type,'oxide')
                o.E = 0;
                o.R_T = 0;
                o.P = 0;
                o.Q = 0;
            end
        end
    end
end