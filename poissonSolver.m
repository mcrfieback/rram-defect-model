function u = poissonSolver(initialGuess, isBoundary, g, dx)
% POISSONSOLVER Solves a three-dimensional poisson equation.

%% Verify arguments
    if ndims(initialGuess) ~= 3
        error('Initial guess must be three-dimensional.');
    end
    if any(size(initialGuess) ~= size(g))
        error('Initial guess must have same dimensions as solution')
    end
    if any(size(initialGuess) ~= size(isBoundary))
        error('Initial guess must have same dimensions as boundaries')
    end
    if any(mod(size(initialGuess),2))
        error('All dimensions must be divisable by 2. It''s gory, I know, sorry.')
    end
    
    
    %% Set parameters
    dispProgress = checkIfDebugging('poissonSolver');
    
    omega = 1.6;
    
    u = initialGuess;
    
    [M,N,P] = size(u);
    maxIter = 5000;
    conversionGoal = 1e-12;
    parity = false;
%     maxCorrVec = zeros(maxIter,1);
    
    %% Run SOR
    for run = 1:maxIter
        maxCorr = 0;
        for nn = 1:2
            % For both parities
            for ii = 1:M
                for jj = 1:N
                    for kk = 1+parity:2:P-~parity
                        if isBoundary(ii,jj,kk)
                            continue;
                        end

                        u_ijk = u(ii,jj,kk);

                        %% Check for grid edge and set neighbor values
                        % For X direction
                        if ii == 1
                            u_imin1 = u_ijk;
                        else
                            u_imin1 = u(ii-1,jj,kk);
                        end
                        if ii == M
                            u_iplus1 = u_ijk;
                        else
                            u_iplus1 = u(ii+1,jj,kk);
                        end

                        % For Y direction
                        if jj == 1
                            u_jmin1 = u_ijk;
                        else
                            u_jmin1 = u(ii,jj-1,kk);
                        end
                        if jj == N
                            u_jplus1 = u_ijk;
                        else
                            u_jplus1 = u(ii,jj+1,kk);
                        end

                        % For Z direction
                        if kk == 1
                            u_kmin1 = u_ijk;
                        else
                            u_kmin1 = u(ii,jj,kk-1);
                        end
                        if kk == P
                            u_kplus1 = u_ijk;
                        else
                            u_kplus1 = u(ii,jj,kk+1);
                        end

                        %% Calculate correction
                        c = omega*((-dx^2 * g(ii,jj,kk) ...
                                    + u_imin1 + u_iplus1 ...
                                    + u_jmin1 + u_jplus1 ...
                                    + u_kmin1 + u_kplus1)/6 - u_ijk);

                        u(ii,jj,kk) = u_ijk + c;

                        if abs(c) > maxCorr
                            maxCorr = abs(c);
                        end
                    end
                    parity = ~parity;
                end
                parity = ~parity;
            end
            parity = ~parity;
        end
        
%         maxCorrVec(run) = maxCorr;
        if maxCorr < conversionGoal
            break;
        end
        if dispProgress && ~mod(run,100)
            disp([num2str(run) '/' num2str(maxIter)]);
        end
    end
    
    if run == maxIter
        warning(['Reached max iterations: solution might be inaccurate. (Final max corr: ' ...
            num2str(maxCorr) '; conversion: ' num2str(conversionGoal) ')']);
    end
    
    if dispProgress
        disp(['Exited after ' num2str(run) ' runs.']);
    end
end