classdef model < handle
    properties
        %% Variable model properties
        N_x     %       Data points on x
        N_y     %       Data points on y
        N_z     %       Data points on z
        
        T_amb   % [K]   Ambient temperature
        V_U     % [V]   Upper electrode voltage
        V_L     % [V]   Lower electrode voltage
        I       % [A]   Current through the cell
        
        grid    %       List of lattice elements
        
        paths
    end
    
    methods (Access=public)
        %% Constructor
        function o = model(V_U,V_L,T_amb,lat_props)
            % MODEL Constructor of the RRAM physical model.
            % Accepts upper/lower electrode voltage, ambient temperature,
            % and a list of lattice properties.
            % (see: setProps, buildLatticeGrid)
            
            o.V_U = V_U;
            o.V_L = V_L;
            o.T_amb = T_amb;

            o.N_x = lat_props.N_x;
            o.N_y = lat_props.N_y;
            o.N_z = lat_props.N_z;

            o.grid = buildLatticeGrid(lat_props,V_U,V_L,T_amb);
            
            % Update potential, power and temperature.
            o.updatePotential;
            o.getCurrent;
            o.updateTemperature;
        end
        
        %% Do a step
        function [dt,I,e] = doStep(o,dt_limit)
            % DOSTEP Advances the model by a timestep. Accepts a timestep
            % limit to prevent too long timesteps and returns the kMC
            % timestep, device current and occurred event.
            showText = checkIfDebugging('doStep');
            
            % NB: In the model handler, the voltage should be updated, and
            % thus the potential and electric field, before every step. To
            % save time, it has been left out here, but it must be updated
            % before TAT is run to retrieve proper phi and F for current.
            
            % Calculate current, dissipated power and vacancy charges
            if showText; disp('Solving TAT current...'); end
            [I_TAT,I_DT] = o.getCurrent;
            
            I = [(I_TAT+I_DT) I_TAT I_DT];
            
            % Bypass kMC
            dt = dt_limit;
            e = 0;
            
%             % Update potential map given new grid from TAT for kMC
%             if showText; disp('Updating potential map...'); end
%             o.updatePotential;
%             
%             % Update temperature map given new grid from TAT for kMC
%             if showText; disp('Updating temperature map...'); end
%             o.updateTemperature;
%             
%             % Use kMC to find new event
%             if showText; disp('Finding next event...'); end
%             [event, dt] = kMCDecider(o.grid);
%             
%             % Determine whether to do event now or discard it if rate too low > timestep too large
%             if dt > dt_limit
%                 disp(['Timestep too large (' num2str(dt) ' s), nothing happens']);
%                 dt = dt_limit;
%                 e = 0;
%             else
%                 if showText; disp(['Executing event ' event.type ' to ' num2str(event.to)]); end
%                 o.doEvent(event)
%                 e = find(strcmp(event.type,{'G_F','R_Di','R_R'}));
%             end
        end
        
        %% Current solver
        % TODO: Move to private when debugged properly
        function [I_TAT,I_DT] = getCurrent(o)
            % GETCURRENT Calculates TAT current and set power and charge.
            [I_TAT,P_TAT,Q,o.paths] = TATSolver(o.grid);
            [I_DT,P_DT] = DTSolver(o.grid);
            
            o.paths = sortrows(o.paths,2,'descend');
            for n = 1:numel(o.grid)
                o.grid(n).P = P_TAT(n) + P_DT(n);
                o.grid(n).Q = Q(n);
            end
        end
        
        %% Grid updating functions
        % TODO: Move to private when debugged properly
        function updatePotential(o)
            % UPDATEPOTENTIAL Updates the local potential using the 3D
            % Poisson equation solver from the charge density.
            global PROPS;
            
            epsilon_0 = 8.854e-12;  % Dielectric constant of vacuum

            % Get old grid potential (as column vector)
            phi = [o.grid.phi]';
            phi_ex = [o.grid.phi_ex]';
            
%             o.updateMetallization;
            
            % Build solution function from charge density
            % from: int_V -rho/epsilon (rho for +1e charge)
            b = [o.grid.Q]' ./ (PROPS.epsilon*epsilon_0);
            
            % Get adjacency matrix sparsified rows
            A = {o.grid.A_row}';
            % Find boundary points and wipe adjacency rows
            isBoundary = ~[o.grid.isIL] & o.grid.is({'upper','OEL','lower'});
            for n = find(isBoundary); A{n} = []; end
            
            
            % Find new potential with Poisson solver
            phiNew = poissonSolver2(A, phi, b);
            phi_exNew = poissonSolver2(A, phi_ex, zeros(size(b)));
            % Update the grid potentials
            for n = 1:numel(o.grid)
                o.grid(n).phi = phiNew(n);
                o.grid(n).phi_ex = phi_exNew(n);
            end
            % Update the electric field
            o.updateElectricField;
        end
        
        function updateTemperature(o)
            % UPDATETEMPERATURE Updates the local temperature using the 3D
            % Poisson equation solver from the power density.
            global PROPS;
            k_th = PROPS.k_th;
            
            T = [o.grid.T]';
            % Build solution function from power density
            % from: int_V P/k_th
            b = [o.grid.P]'/(k_th);
            
            % Get adjacency matrix sparsified rows
            A = {o.grid.A_row}';
            % Find boundary points and wipe adjacency rows
            isBoundary = o.grid.is({'upper','lower'});
            for n = find(isBoundary); A{n} = []; T(n) = o.T_amb; end
            
            % Find solution to Fourier equation with Poisson solver
            TNew = poissonSolver2(A, T, b);
            % Update the temperature field
            for n = 1:numel(o.grid); o.grid(n).T = TNew(n); end
        end
        
        function updateVoltage(o,V_U,V_L)
            % UPDATEVOLTAGE Updates the voltage of the electrodes by
            % setting phi and phi_ex to the provided potentials.
            
            % Set the upper electrode and OEL voltage
            if ~isempty(V_U)
                o.V_U = V_U;
                for ii = find(o.grid.is({'upper', 'OEL'}))
                    o.grid(ii).phi = V_U;
                    o.grid(ii).phi_ex = V_U;
                end
            end
            % Set the lower electrode voltage
            if ~isempty(V_L)
                o.V_L = V_L;
                for ii = find(o.grid.is('lower'))
                    o.grid(ii).phi = V_L;
                    o.grid(ii).phi_ex = V_L;
                end
            end
            
            % Update the grid potentials
            o.updatePotential;
        end
        
        function updateElectricField(o)
            % UPDATEELECTRICFIELD Updates the electric vector field by
            % (sparse) multiplying the precalculated Nabla gradient matrix
            % with the potential vectors.
            for n = 1:numel(o.grid)
                gp = o.grid(n);
                F_vec = [0;0;0];
                F_vec_ex = [0;0;0];
                for nn = 1:size(gp.Nabla_row,1)
                    F_vec = F_vec - gp.Nabla_row(nn,2:4)' .* o.grid(gp.Nabla_row(nn,1)).phi; 
                    F_vec_ex = F_vec_ex - gp.Nabla_row(nn,2:4)' .* o.grid(gp.Nabla_row(nn,1)).phi_ex; 
                end
                gp.F_vec = F_vec;
                gp.F_vec_ex = F_vec_ex;
                gp.F = sqrt(F_vec'*F_vec);
                gp.F_ex = sqrt(F_vec_ex'*F_vec_ex);
                o.grid(n) = gp;
            end
        end
        
        function updateMetallization(o)
            % UPDATEMETALLIZATION Updates the local charge to model lattice
            % metallization and reduce the local field in large vacancy
            % clusters.
            
            trapIndices = find(o.grid.is('V+2'));
            
            for ii = trapIndices
                o.grid(ii).Q = 2*1.602e-19;
            end
            
            max_d = 1e-9;
            k_d = 0.5;
            
            n = 1;
            
            for ii = trapIndices
                n = n+1;
                for jj = trapIndices
                    d = sqrt((o.grid(ii).x - o.grid(jj).x)^2 ...
                           + (o.grid(ii).y - o.grid(jj).y)^2 ...
                           + (o.grid(ii).z - o.grid(jj).z)^2);
                       
                    if d < max_d
                        k = d/max_d*k_d + 1 - k_d;
                    else
                        k = 1;
                    end
                    o.grid(jj).Q = o.grid(jj).Q*k;
                end
            end
        end
        
        function updatePermittivity(o)
            % UPDATEPERMITTIVITY Updates the local relative dielectric
            % permittivity from the number of vacancies in a vertical (z)
            % chunk.
            
            global PROPS;

            for ii = 1:o.N_x
                for jj = 1:o.N_y
                    n = sub2ind([o.N_x o.N_y o.N_z],repmat(ii,[1 o.N_z]),repmat(jj,[1 o.N_z]),1:o.N_z);
                    vc = nnz(o.grid(n).is('V+2'));
                    new_eps = PROPS.epsilon + PROPS.k_ep*vc;
                    for kk = 1:o.N_z
                        o.grid(n(kk)).epsilon = new_eps;
                        if any(o.grid(n(kk)).intAt)
                            for nn = 1:8
                                if o.grid(n(kk)).intAt(nn) > 0
                                    o.grid(o.grid(n(kk)).intAt(nn)).epsilon = new_eps;
                                end
                            end
                        end
                    end
                end
            end
        end
        

        %% KMC functions
        function doEvent(o,event)
            % DOEVENT Executes the provided event structure on the model.
            % Event needs fields: type, index and to.
            showEvents = checkIfDebugging('doEvent');
            
            % Grid size
            gs = [o.N_x o.N_y o.N_z];
            
            % Event point
            gp = o.grid(event.index);
            [ii, jj, kk] = ind2sub(gs,event.index);
            
            % Check if event reaches across the edge of the lattice grid
            acrossEdge = [
                ii == 1
                jj == 1
                kk == 1
                ii == o.N_x
                jj == o.N_y
                kk == o.N_z
            ];
            acrossEdge = acrossEdge(event.to);
            
            % If not, then find the target index
            if ~acrossEdge
                switch event.to
                    case 1; targetIndex = sub2ind(gs,ii-1,jj,kk);
                    case 2; targetIndex = sub2ind(gs,ii,jj-1,kk);
                    case 3; targetIndex = sub2ind(gs,ii,jj,kk-1);
                    case 4; targetIndex = sub2ind(gs,ii+1,jj,kk);
                    case 5; targetIndex = sub2ind(gs,ii,jj+1,kk);
                    case 6; targetIndex = sub2ind(gs,ii,jj,kk+1);
                    otherwise; error('Unknown direction: %d', event.to);
                end
            end
            
            % Direction names
            dirNames = {'-x','-y','-z','+x','+y','+z'};

            switch (event.type)
                case 'G_F'
                % Generate a V+/O- Frenkel pair
                
                % Generate V+
                o.grid(event.index) = gp.turnInto('V+2');
                
                % Generate O-
                if acrossEdge
                    warning('O-2 generated off grid at [%d].',event.index); 
                else
                    if o.grid(targetIndex).is('OEL')
                        o.grid(targetIndex) = o.grid(targetIndex).turnInto('OEL_ox');
                        if showEvents; fprintf('Generation at [%d] to %s, gettered by OEL\n',event.index,dirNames{event.to}); end
                    else
                        o.grid(targetIndex) = o.grid(targetIndex).turnInto('O-2');
                        if showEvents; fprintf('Generation at [%d] to %s\n',event.index,dirNames{event.to}); end
                    end
                end
                    
                case 'R_Di'
                % Move an oxygen ion
                o.grid(event.index) = o.grid(event.index).turnInto('oxide');
                
                if acrossEdge
                    % Remove O-2 with a warning
                    warning('O-2 [%d] drifted off grid',event.index);
                else
                    if o.grid(targetIndex).is('OEL')
                        o.grid(targetIndex) = o.grid(targetIndex).turnInto('OEL_ox');
                        if showEvents; fprintf('O-2 [%d] drifted to %s, gettered by OEL\n',event.index,dirNames{event.to}); end
                    else
                        o.grid(targetIndex) = o.grid(targetIndex).turnInto('O-2');
                        if showEvents; fprintf('O-2 [%d] drifted to %s\n',event.index,dirNames{event.to}); end
                    end
                end
                
                case 'R_R'
                % Recombine a V+/O- pair
                
                % Turn event O- into oxide
                o.grid(event.index) = o.grid(event.index).turnInto('oxide');
                % Turn target V+ into oxide
                o.grid(targetIndex) = o.grid(targetIndex).turnInto('oxide');
                
                if showEvents
                    fprintf('O-2 recombined into [%d]\n',targetIndex);
                end
            end
        end
        
        %% Plotting functions
        function plotGrid(o,parameter)
            % PLOTGRID Plots any value of the grid in a suitable plot.
            global PROPS;
            
            xs = min([o.grid.x]); xe = max([o.grid.x]);
            ys = min([o.grid.y]); ye = max([o.grid.y]);
            zs = min([o.grid.z]); ze = max([o.grid.z]);
            
            if ~exist('parameter','var')
                parameter = 'type';
            end
            
            switch (parameter)
                case 'type'
                    types = {'oxide', 'lower', 'upper', 'OEL', 'OEL_ox', 'O-2', 'V+2','air'};
                    % Draw only the important stuff (see-through the oxide)
                    pos = cell(length(types)-1,1);
                    typesInLegend = false(size(types));
                    
                    % Retrieve charge for all vacancies and normalize
                    Qvb = [o.grid(o.grid.is('V+2')).Q];
                    Qvb = (Qvb/1.602e-19)/2;
                    
                    for n = 1:length(types)-1
                        % Retrieve xyz pos for all of every type in types
                        allThisType = o.grid(o.grid.is(types{n+1}));
                        if isempty(allThisType)
                            pos{n} = zeros(0,3);
                        else
                            % Only show legend for types that are present
                            typesInLegend(n+1) = true;
                            pos{n} = [
                                [allThisType.x];
                                [allThisType.y];
                                [allThisType.z];
                                ]';
                        end
                    end
                    % Draw the types with the retrieved xyz:
                    % lower
                    plot3(pos{1}(:,1),pos{1}(:,2),pos{1}(:,3),'k^', ...
                        'MarkerSize',10,'MarkerFaceColor',[0.4 0.4 0.4])
                    hold on;
                    % upper
                    plot3(pos{2}(:,1),pos{2}(:,2),pos{2}(:,3),'kv', ...
                        'MarkerSize',10,'MarkerFaceColor','none');
                    % OEL
                    plot3(pos{3}(:,1),pos{3}(:,2),pos{3}(:,3),'ko', ...
                        'MarkerSize',3,'MarkerFaceColor',[0.6 0.6 0.6]);
                    % OEL_ox
                    plot3(pos{4}(:,1),pos{4}(:,2),pos{4}(:,3),'ko', ...
                        'MarkerSize',10,'MarkerFaceColor',[1 1 1]);
                    % O-2
                    plot3(pos{5}(:,1),pos{5}(:,2),pos{5}(:,3),'ko', ...
                        'MarkerSize',10,'MarkerFaceColor','r');
                    % V+2
                    for n = 1:length(Qvb)
                    plot3(pos{6}(n,1),pos{6}(n,2),pos{6}(n,3),'ko', ...
                        'MarkerSize',10,'MarkerFaceColor',[1-Qvb(n) 1-Qvb(n) 1]);
                    end
                    % air
                    plot3(pos{7}(:,1),pos{7}(:,2),pos{7}(:,3),'r*', ...
                        'MarkerSize',10);
                    
                    % Plot the direction of field for all ions
                    ions = o.grid(o.grid.is('O-2'));
                    if ~isempty(ions)
                        F_vecs = [ions.F_vec]*1e-18;
                        Fx = [ions.x];
                        Fy = [ions.y];
                        Fz = [ions.z];
                        Fu = F_vecs(1,:);
                        Fv = F_vecs(2,:);
                        Fw = F_vecs(3,:);
                        quiver3(Fx,Fy,Fz,Fu,Fv,Fw,'Color','r');
                    end
                    
                    Iv = cell2mat(o.paths(:,2));
                    Ivr = max(Iv)-min(Iv);
                    if Ivr == 0; Ivr = 1; end
                    Ivn = round(4*(Iv - min(Iv))/Ivr) + 1;
                    
                    if size(o.paths,1) > 5
                        N = 5;
                    else
                        N = size(o.paths,1);
                    end
                    
                    for n = 1:N
                        path = o.paths{n,1};
                        plot3([o.grid(path).x],[o.grid(path).y],[o.grid(path).z],'k:','LineWidth',Ivn(n));
                    end
                    
                    hold off;
                    view(-45,10)
                    
                case 'F_vec'
                % Create a quiver plot that shows the electric field as a vector field
                    F_vecs = [o.grid.F_vec];
                    Fx = [o.grid.x];
                    Fy = [o.grid.y];
                    Fz = [o.grid.z];
                    Fu = F_vecs(1,:);
                    Fv = F_vecs(2,:);
                    Fw = F_vecs(3,:);
                    quiver3(Fx,Fy,Fz,Fu,Fv,Fw);
                    line( ...
                        [xs xe xe xs xs], ...
                        [ys ys ye ye ys], ...
                        [zs zs zs zs zs]  ...
                        );
                    line( ...
                        [xs xe xe xs xs], ...
                        [ys ys ye ye ys], ...
                        [ze ze ze ze ze]  ...
                        );
                    view(-45,10)
                case 'N_VI'
                % Plot vacancy/ion density vs. z coordinate
                    
                    % Count vacancies/ions per z layer
                    isVacancy = o.grid.is('V+2');
                    isIon = o.grid.is('O-2');
                    
                    N_V = zeros(o.N_z,1);
                    N_I = zeros(o.N_z,1);
                    zl = linspace(xs,xe,o.N_z);
                    zs = zl(2)-zl(1);
                    
                    for n = 1:o.N_z-1
                        isInLayer = ([o.grid.z] > zl(n)-zs/2) & ([o.grid.z] < zl(n)+zs/2);
                        N_V(n) = nnz(isVacancy & isInLayer);
                        N_I(n) = nnz(isIon & isInLayer);
                    end
                    
                    % Get unit volume
                    lat_a = PROPS.lat_a*1e-10;      % [m] Lattice shape parameter a
                    lat_b = PROPS.lat_b*1e-10;      % [m] Lattice shape parameter b
                    lat_c = PROPS.lat_c*1e-10;      % [m] Lattice shape parameter c
                    lat_gamma = PROPS.lat_gamma;    % [deg] Lattice shape parameter gamma

                    av = [lat_a 0 0]; % Vector along x lattice edge
                    bv = [0 lat_b 0]; % Vector along y lattice edge
                    cv = [lat_c*cos(deg2rad(lat_gamma)) 0 lat_c*sin(deg2rad(lat_gamma))]; % Vector along z lattice edge
                    
                    unitV = dot(cross(av,bv), cv);
                    
                    % Convert number of vacancies/ions to density per cm^3
                    N_V = N_V / (o.N_x*o.N_y*unitV*1e6);
                    N_I = N_I / (o.N_x*o.N_y*unitV*1e6);
                    
                    plot(zl*1e9,N_V,'b',zl*1e9,N_I,'r');
                    xlabel('z [nm]');
                    ylabel('Concentration [cm^{-3}]');
                    return;
                otherwise
                % Just plot the data from the parameter
                    x = [o.grid.x]; y = [o.grid.y]; z = [o.grid.z];
                    vs = [o.grid.(parameter)];
                    gs = [o.N_x o.N_y o.N_z];
                    plotBlockData2(x,y,z,vs,gs);
            end
            xlabel('x');
            ylabel('y');
            zlabel('z');
        end
        
        
        %% Debug functions
        function values = debugCollectFromGrid(o,parameter)
            values = cell(o.N_x,o.N_y,o.N_z);
            for ii = 1:o.N_x
                for jj = 1:o.N_y
                    for kk = 1:o.N_z
                        values{ii,jj,kk} = o.grid(ii,jj,kk).(parameter);
                    end
                end
            end
        end
    end
end
