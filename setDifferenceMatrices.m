function grid = setDifferenceMatrices(grid, W, updateNodes)

    updateMode = exist('updateNodes','var');
    if updateMode
        debugDisp('Update mode on, clearing nodes...')
        % Clear the matrix rows of the to be updated nodes.
        for n = updateNodes
            grid(n).A_row = zeros(0,2);
            grid(n).Nabla_row = zeros(0,4);
        end
        debugDisp(['Cleared nodes ' num2str(updateNodes,' %d') '.'])
    else
        debugDisp('Update mode off, starting from scratch...')
        % All nodes should be updated.
        updateNodes = 1:numel(grid);
    end

    adjElem = zeros(numel(grid),1); % Amount of tetrahedra adjacent to vertex
    
    debugDisp('Building matrices from elements...')
    
    for n = 1:size(W,1)
        if ~updateMode && mod(n,1000) == 0; debugDisp(['  Element ' num2str(n) '/' num2str(size(W,1)) '...']); end
        vs = W(n,:); % The four indices of grid points in the tetrahedron
        % Skip to next element if no update is necessary
        skipElem = true;
        for d = 1:4
            if any(updateNodes == vs(d)); skipElem = false; break; end
        end
        if updateMode && skipElem; continue; end
        
        % Get the vertex coordinates as a (4x1) vector
        vx = [grid(vs).x]'; vy = [grid(vs).y]'; vz = [grid(vs).z]';
    
        e = [ones(4,1) vx vy vz];
        ei = inv(e); ei = ei(2:4,:);
        s = ei'*ei*abs(det(e))/6;

        for ii = 1:4
            % Don't touch grid points that do not need to be updated
            if updateMode && ~any(updateNodes == vs(ii)); continue; end
            
            adjElem(vs(ii)) = adjElem(vs(ii)) + 1;
            for jj = 1:4
                % Normal gradient matrix magnitude is 0.1/L_step (e8)
                if max(abs(ei(:,jj))) > 1e5
                    ind = grid(vs(ii)).Nabla_row(:,1) == vs(jj);
                    if any(ind)
                        grid(vs(ii)).Nabla_row(ind,2:4) = grid(vs(ii)).Nabla_row(ind,2:4) + ei(:,jj)';
                    else
                        grid(vs(ii)).Nabla_row = [grid(vs(ii)).Nabla_row; vs(jj) ei(:,jj)'];
                    end
                end
                
                % Normal stiffness matrix magnitude is L_step (e-10)
                if abs(s(ii,jj)) > 1e-13
                    ind = grid(vs(ii)).A_row(:,1) == vs(jj);
                    if any(ind)
                        grid(vs(ii)).A_row(ind,2) = grid(vs(ii)).A_row(ind,2) + s(ii,jj);
                    else
                        grid(vs(ii)).A_row = [grid(vs(ii)).A_row; vs(jj) s(ii,jj)];
                    end
                end
            end
        end
    end
    
    debugDisp('Scaling Nabla rows...');
    for n = updateNodes
        grid(n).Nabla_row(:,2:4) = grid(n).Nabla_row(:,2:4) / adjElem(n);
    end
    
    debugDisp('Set Difference Matrices done.');
end

function debugDisp(text)
    if checkIfDebugging('setDifferenceMatrices')
        disp(text);
    end
end