function [I,P,Q,paths] = TATSolver_old(grid)
    % TATSOLVER Calculates the current through a grid of lattice_elements
    % assuming Trap Assisted Tunneling as the main conduction method.
    % Direct Tunneling is calculated in another function, DTSolver.

    global PROPS;
    props = struct();
    for fn = {'m_e','hbarOmega0_ox','hbarOmega0_il','phi_g0_ox','phi_g0_il','E_F_ox','E_F_il','S'}
        props.(fn{1}) = PROPS.(fn{1});
    end
    
    showDebug = checkIfDebugging('TATSolver');
    
    q = 1.602e-19;
    P = [grid.P];
    Q = [grid.Q];
    f = zeros(size(grid));
    paths = cell(0,3);
    
    % Build a list out of the vacancies in the grid
    traps_unsort = grid(strcmp({grid.type},'V+2'))';
    N_traps = length(traps_unsort);
    
    % Order the traps from lowest to highest potential (movement of electrons)
    [~,ind] = sort([traps_unsort.phi_ex]);
    traps = traps_unsort(ind);
    
    % Get the upper and lower electrode potentials
    V_U = grid(find(strcmp({grid.type},'upper') & ~[grid.isIL], 1)).phi_ex;
    V_L = grid(find(strcmp({grid.type},'lower') & ~[grid.isIL], 1)).phi_ex;
    
    % Check which electrode is the anode
    isLowerAnode = V_U < V_L;
    
    if V_U == V_L
        I = 0;
        P = zeros(size(P));
        return;
    end
    
    % Calculate all rates from the cathode and to the anode
    R_ca = zeros(N_traps,1); % Rates from cathode to trap ii
    R_an = zeros(N_traps,1); % Rates from trap ii to anode
    for ii = 1:N_traps
        if isLowerAnode
            cathode = grid(traps(ii).index_U);
            anode = grid(traps(ii).index_L);
        else
            cathode = grid(traps(ii).index_L);
            anode = grid(traps(ii).index_U);
        end
        [R_ca(ii),f(traps(ii).index)] = getRate(cathode,traps(ii),props);
        [R_an(ii),f(anode.index)] = getRate(traps(ii),anode,props);
    end
    
    % Save the trap list indices to retrieve the correct max index
    trapListIndex = 1:N_traps;
    
    % Boolean vector that flags traps as being in a path
    trapInPath = false(length(traps),1);
    
    N_paths = 0;
    I = 0;

    while any(~trapInPath)
        % First trap considered is highest rate cathode --> trap
        % (This needs to be done in 3 lines because MATLAB changes indices of logically indexed matrices...)
        [R_min, reducedNextTrap] = max(R_ca(~trapInPath));  % First find max index in (reduced) R_ca
        reducedTrapListIndex = trapListIndex(~trapInPath);  % Build similarly reduced trap list index
        nextTrap = reducedTrapListIndex(reducedNextTrap);   % Retrieve correct index from the list
        
        if (R_min == 0)
            if showDebug; disp('No more traps can be reached from the cathode, exiting...'); end
            break;
        end

        path = [0 nextTrap];
        N_paths = N_paths + 1;
        
        if showDebug
            progress = 100*nnz(trapInPath)/length(trapInPath);
            disp(['Calculating path ' num2str(N_paths) '... [' num2str(progress) '%]'])
        end

        k = 0;
        
        while nextTrap ~= N_traps+1 % While path has not arrived at anode
            k = k + 1;
            % Move to the next trap
            currentTrap = nextTrap;
            trapInPath(currentTrap) = true;
            
            % Calculate rates from currentTrap to all other traps not in the path
            R_ctrap = zeros(N_traps,1);
            for ii = (currentTrap+1):N_traps
                if ~trapInPath(ii)
                    [R_ctrap(ii),f(traps(ii).index)] = getRate(traps(currentTrap),traps(ii),props);
                end
            end

            % Find the next trap in the path
            % (Similarly in 3 lines due to same issue as before)
            [R_next, nextTrap] = max(R_ctrap);
%             [R_next, reducedNextTrap] = max(R_ctrap(~trapInPath));
%             reducedTrapListIndex = trapListIndex(~trapInPath);
%             nextTrap = reducedTrapListIndex(reducedNextTrap);

            % Check if either:
            % - All traps are in a path
            % - Rate to anode is larger than to unmarked traps
            % - There is no rate to unmarked traps > 0
            % If so, set exit condition
            if isempty(R_next) || (R_an(currentTrap) > R_next) || (R_next == 0)
                R_next = R_an(currentTrap);
                nextTrap = N_traps+1;
            end
            
            if (nextTrap ~= N_traps+1) && (trapInPath(nextTrap))
                error('Selected a trap that is already in the path.');
            end
            
            path = [path nextTrap];

            % Set the new minimum rate if it is smaller
            if R_next < R_min
                R_min = R_next; 
                paths{N_paths,3} = k+1;
            end
        end
        I_path = q*R_min;
        I = I + I_path;
        
        Q([traps.index]) = q*(1-f([traps.index]));
        
        % Set dissipated power
        firstTrap = traps(path(2));     % The first trap of the path
        lastTrap = traps(path(end-1));  % The last trap of the path
        if isLowerAnode
            % The start is the upper electrode
            pathStart = grid(firstTrap.index_U);
            pathEnd = grid(lastTrap.index_L);
        else
            % The start is the lower electrode
            pathStart = grid(firstTrap.index_L);
            pathEnd = grid(lastTrap.index_U);
        end
        % Set the power dissipated in the first trap
        P(firstTrap.index) = R_min*(q+(-q*pathStart.phi_ex-pathStart.E) - (-q*firstTrap.phi_ex-firstTrap.E));
        % Set the power dissipated in the electrode (IL) at the end
        P(pathEnd.index) = R_min*(q+(-q*lastTrap.phi_ex-lastTrap.E) - (-q*pathEnd.phi_ex-pathEnd.E));
        % Set the power dissipated in the rest of the traps in the path
        for step = 3:(length(path)-1)
            P(traps(path(step)).index) = R_min*(q+(-q*traps(path(step-1)).phi_ex-traps(path(step-1)).E) ...
                - (-q*traps(path(step)).phi_ex-traps(path(step)).E));
        end
        
        P(P<0) = 0;
        
        
        if showDebug; disp(['I_path = ' num2str(I_path) '; path: ' num2str(path)]); end
        paths{N_paths,1} = path;
        paths{N_paths,2} = q*R_min;
    end
    
    if showDebug
        disp(['Total current is ' num2str(I) 'A through ' num2str(N_paths) ' percolation paths.']);
    end
    
    if any(P < 0,'all')
        warning('Detected negative power.');
    end
    
    if checkIfDebugging('TATSolver plot')
        pmin = min([grid.x]);
        xmax = max([grid.x]);
        ymax = max([grid.y]);
        zmax = max([grid.z]);
        
        oldpathx = NaN(N_traps+2,N_paths);
        oldpathy = NaN(N_traps+2,N_paths);
        oldpathz = NaN(N_traps+2,N_paths);
        
        trapLabels = cell(N_traps,1);
        
        stackArea = (pmin)^2*(size(grid,1)*size(grid,2));
        
        for n = 1:N_traps; trapLabels{n} = num2str(n); end

        for n = 1:N_paths
            path = paths{n,1};
            botnLoc = paths{n,3};
            if isLowerAnode
                pathx = [grid(traps(path(2)).index_U).x traps(path(2:end-1)).x grid(traps(path(end-1)).index_L).x];
                pathy = [grid(traps(path(2)).index_U).y traps(path(2:end-1)).y grid(traps(path(end-1)).index_L).y];
                pathz = [grid(traps(path(2)).index_U).z traps(path(2:end-1)).z grid(traps(path(end-1)).index_L).z];
                icon = 'v';
            else
                pathx = [grid(traps(path(2)).index_L).x traps(path(2:end-1)).x grid(traps(path(end-1)).index_U).x];
                pathy = [grid(traps(path(2)).index_L).y traps(path(2:end-1)).y grid(traps(path(end-1)).index_U).y];
                pathz = [grid(traps(path(2)).index_L).z traps(path(2:end-1)).z grid(traps(path(end-1)).index_U).z];
                icon = '^';
            end

            botnx = pathx([botnLoc botnLoc+1]);
            botny = pathy([botnLoc botnLoc+1]);
            botnz = pathz([botnLoc botnLoc+1]);

            plot3([traps.x],[traps.y],[traps.z],'bx', ...
                  oldpathx, oldpathy, oldpathz, 'k:*', ...
                  pathx,pathy,pathz,[icon 'm--'], ...
                  botnx,botny,botnz,'r' ...
                  );
            
            title(['I_{perc} = ' num2str(paths{n,2}) 'A'])
            patch([pmin xmax xmax pmin],[pmin pmin ymax ymax],[pmin pmin pmin pmin])
            line([pmin xmax xmax pmin pmin],[pmin pmin ymax ymax pmin],[zmax zmax zmax zmax zmax]);
            text([traps.x],[traps.y],[traps.z]+pmin/2,trapLabels);
            view(340,10);
            pause(0.2);
            
            oldpathx(1:length(pathx),n) = pathx;
            oldpathy(1:length(pathy),n) = pathy;
            oldpathz(1:length(pathz),n) = pathz;
            
            paths{n,1} = [pathStart.index traps(path(2:end-1)).index pathEnd.index];
        end
        
        plot3([traps.x],[traps.y],[traps.z],'bx', ...
              oldpathx, oldpathy, oldpathz, 'k:*' ...
              );
        title(['I_{tot} = ' num2str(I) 'A; J_{tot} = ' num2str(I/stackArea * 1e-4,'%10.4e') 'A/cm^2']);
        patch([pmin xmax xmax pmin],[pmin pmin ymax ymax],[pmin pmin pmin pmin])
        line([pmin xmax xmax pmin pmin],[pmin pmin ymax ymax pmin],[zmax zmax zmax zmax zmax]);
            text([traps.x],[traps.y],[traps.z]+pmin/2,trapLabels);
        view(340,10);
    end
end

function [R,f] = getRate(from,to,props)
    % Get the rate of electrons passing from trap "from" to trap "to"
    
    q = 1.602e-19;
    hbarOmega0 = props.hbarOmega0_ox * q;
    m_e = props.m_e*9.110e-31;  % Effective electron mass
    props.d = sqrt((from.x-to.x)^2 + (from.y-to.y)^2 + (from.z-to.z)^2);
    
    M = 100; % Limit of phonon sum

    tau_c = 0; tau_e = 0;
    
    for n = 0:M
        % Capture and emission rates
        [~, Em_j_n] = calc_CaEm(from,n,props);
        [Ca_j_n, ~] = calc_CaEm(to,n,props);
        % Density of states
        N = calc_N(from,to,n,props);
        if imag(N) > 0; warning(['Complex N detected from ' num2str(from.index) ' to ' num2str(to.index) '(' num2str(n) ' phonons)']); end
        % Phonon occupation probability
        f = calc_f(from,to,n,props);
        % Tunneling probability
        P_T = getTunnelingProbability(from,to,n*hbarOmega0+q,m_e);
%         fprintf('[%d > %d][%d] P_T = %.3f\n',from.index,to.index,n,P_T);
        if imag(P_T) > 0; warning(['Complex P_T detected from ' num2str(from.index) ' to ' num2str(to.index) '(' num2str(n) ' phonons)']); end

        % (inverted) Capture and emission times for n phonons
        tau_c_j_n = N * (1-f) * Ca_j_n * P_T;
        tau_e_j_n = N * (1-f) * Em_j_n * P_T;

        % Add to total (inverted) capture and emission times
        tau_c = tau_c + tau_c_j_n;
        tau_e = tau_e + tau_e_j_n;
    end
    
    % Set the charge rate
    R = 1/(1/tau_c + 1/tau_e);
    f = R/tau_e;
    
    
    if isnan(R)
        disp oop;
    end
end

function N = calc_N(from,to,n,props)
    % Calculate the density of states at the energy of an electron from 
    % trap "from" in trap "to"
    
    q = 1.602e-19;              % Elemental charge
    hbar = 1.055e-34;           % Modified Planck constant
    m_e = props.m_e*9.110e-31;  % Electron effective mass
    
    if from.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
    end
    
    energyDifference = (from.phi_ex*q - from.E + q) + n*hbarOmega0 - (-to.phi_ex*q - to.E);
    
    if energyDifference < 0
        N = 0;
    else
        N = 1/(2*pi^2)*(2*m_e)^(3/2)/hbar^3*sqrt(energyDifference);
    end
end

function f = calc_f(from,to,n,props)
    % Calculate the Fermi-Dirac occupation probability at the energy of an
    % electron from trap "from" in trap "to"
    
    q = 1.602e-19;          % Elemental charge
    k = 1.381e-23;          % Boltzmann constant
    
    if from.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
    end
    
    if to.isIL
        E_F = props.E_F_il * props.phi_g0_il * q;
    else
        E_F = props.E_F_ox * props.phi_g0_ox * q;
    end
    
    f = 1/(1 + exp(((from.phi_ex*q - from.E + q) + n*hbarOmega0 - (-to.phi_ex*q - E_F))/(k*to.T)));
end

function [Ca, Em] = calc_CaEm(trap,n,props)
    % Calculate capture and emission rates of trap "trap" given n phonons
    
    q = 1.602e-19;              % Elemental charge
    hbar = 1.055e-34;           % Modified Planck constant
    m_e = props.m_e*9.110e-31;  % Electron effective mass
    k = 1.381e-23;              % Boltzmann constant
    S = props.S;                % Huang-Rhys factor
    
    % Scale relaxation energy according to mutual vacancy distance
%     S = S * (1 - trap.R_T/props.d);
%     if S < 0; S = 0; end
    
    if trap.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
        phi_g0 = props.phi_g0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
        phi_g0 = props.phi_g0_ox * q;
    end
    
    % For electrodes, trap radius is actually 0, but this will only reflect 
    % in the tunneling probability. Since in Vandelli/Larcher c_0 is 
    % assumed a constant, R_T for electrodes is set the same value as traps
    if trap.isIL; R_T = sqrt(1e-18/pi); else; R_T = trap.R_T; end
    
    % Constant dependent on field
    c_0 = ((4*pi)^2)*(R_T^3)*(q^2)*(trap.F_ex^2)*hbar ... 
        / (phi_g0 * 2*m_e);
    
    % Bose function
    f_B = 1/(exp(hbarOmega0/(k*trap.T)) - 1);
    
    % Bessel function
    I_n = besseli(n, 2*S*sqrt(f_B*(f_B+1)));
    
    % Multiphonon transition probability
    L = ((f_B + 1)/f_B)^(n/2)*exp(-S*(2*f_B+1))*I_n;
    
    % Capture rate
    Ca = c_0*L;
    
    % Emission rate
    Em = Ca*exp(-n*hbarOmega0/(k*trap.T));
end
