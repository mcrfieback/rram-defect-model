function [I,P,Q] = TATSolverReduced(grid)
    % TATSOLVER Calculates the current through a grid of lattice_elements
    % assuming Trap Assisted Tunneling as the main conduction method.
    % Direct Tunneling is calculated in another function, DTSolver.

    global PROPS;
    props = struct();
    for fn = {'m_e','hbarOmega0_ox','hbarOmega0_il','phi_g0_el','phi_g0_ox','phi_g0_il','E_F_el','E_F_ox','E_F_il','S'}
        props.(fn{1}) = PROPS.(fn{1});
    end
    props.M = 10; % Limit of phonon sum
    
    showDebug = checkIfDebugging('TATSolver');
    
    q = 1.602e-19;
    P = [grid.P];
    Q = [grid.Q];
    f = zeros(size(grid));
    
    % Build a list out of the vacancies in the grid
    traps_unsort = grid(strcmp({grid.type},'V+2'))';
    N_traps = length(traps_unsort);
    
    % Order the traps from lowest to highest potential (movement of electrons)
    [~,ind] = sort([traps_unsort.phi_ex]);
    traps = traps_unsort(ind);
    
    % Get the upper and lower electrode potentials
    V_U = grid(find(strcmp({grid.type},'upper') & ~[grid.isIL], 1)).phi_ex;
    V_L = grid(find(strcmp({grid.type},'lower') & ~[grid.isIL], 1)).phi_ex;
    
    % Check which electrode is the anode
    isLowerAnode = V_U < V_L;
    
    if V_U == V_L
        I = 0;
        P = zeros(size(P));
        return;
    end
    
    % Calculate all rates from the cathode and to the anode
    R = zeros(N_traps,1); % Rates from cathode via trap ii to anode
    for ii = 1:N_traps
        if isLowerAnode
            cathode = grid(traps(ii).index_U);
            anode = grid(traps(ii).index_L);
        else
            cathode = grid(traps(ii).index_L);
            anode = grid(traps(ii).index_U);
        end
        [R(ii),f(traps(ii).index)] = getRate(cathode,traps(ii),anode,props);
%         % Set the power dissipated in the first trap
%         P(traps(ii).index) = R(ii)*(q+(-q*cathode.phi_ex-cathode.E) - (-q*traps(ii).phi_ex-traps(ii).E));
%         % Set the power dissipated in the electrode (IL) at the end
%         P(anode.index) = R(ii)*(q+(-q*traps(ii).phi_ex-traps(ii).E) - (-q*anode.phi_ex-anode.E));
    end
    
    I = q*sum(R);
    
    if showDebug
        disp(['Total current is ' num2str(I) 'A through ' num2str(N_traps) ' percolation paths.']);
    end
    
    if any(P < 0,'all')
        warning('Detected negative power.');
    end
end

function [R,f] = getRate(from,trap,to,props)
    % Get the rate of electrons passing from trap "from", via trap "trap",
    % to trap "to"
    
    q = 1.602e-19;
    hbarOmega0 = props.hbarOmega0_ox * q;
    m_e = props.m_e*9.110e-31;  % Effective electron mass
    
    M = props.M; % Limit of phonon sum

    tau_c = 0; tau_e = 0;
    
    for n = 0:M
        % Capture and emission rates
        [Ca_j_n, Em_j_n] = calc_CaEm(trap,n,props);
        % Density of states
        N_n = calc_N(from,trap,n,props);
        if imag(N_n) > 0; warning(['Complex N detected from ' num2str(from.index) ' to ' num2str(trap.index) '(' num2str(n) ' phonons)']); end
        N_p = calc_N(to,trap,n,props);
        if imag(N_p) > 0; warning(['Complex N detected from ' num2str(to.index) ' to ' num2str(trap.index) '(' num2str(n) ' phonons)']); end
        % Phonon occupation probability
        f_n = calc_f(trap,from,n,props);
        f_p = calc_f(trap,to,n,props);
        % Tunneling probability
        P_T_n = getTunnelingProbability(from,trap,n*hbarOmega0+q,m_e);
        if imag(P_T_n) > 0; warning(['Complex P_T detected from ' num2str(from.index) ' to ' num2str(to.index) '(' num2str(n) ' phonons)']); end
        P_T_p = getTunnelingProbability(trap,to,n*hbarOmega0+q,m_e);
        if imag(P_T_p) > 0; warning(['Complex P_T detected from ' num2str(from.index) ' to ' num2str(to.index) '(' num2str(n) ' phonons)']); end
        % (inverted) Capture and emission times for n phonons
        tau_c_j_n = N_n * (f_n) * Ca_j_n * P_T_n;
        tau_e_j_n = N_p * (1-f_p) * Em_j_n * P_T_p;

        % Add to total (inverted) capture and emission times
        tau_c = tau_c + tau_c_j_n;
        tau_e = tau_e + tau_e_j_n;
    end
    
    % Set the charge rate
    R = 1/(1/tau_c + 1/tau_e);
    f = R/tau_e;
    
%     fprintf('[%s > %s > %s] - tau_c: %.2e, tau_e: %.2e, f: %.5f\n',from.type,trap.type,to.type,tau_c,tau_e,f);
end

function N = calc_N(from,to,n,props)
    % Calculate the density of states at the energy of an electron from 
    % trap "from" in trap "to"
    
    q = 1.602e-19;              % Elemental charge
    hbar = 1.055e-34;           % Modified Planck constant
    m_e = props.m_e*9.110e-31;  % Electron effective mass
    
    if from.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
    end
    
    energyDifference = (-from.phi_ex*q - from.E + q) + n*hbarOmega0 - (-to.phi_ex*q - to.E);
    
    if energyDifference < 0
        N = 0;
    else
        N = 1/(2*pi^2)*(2*m_e)^(3/2)/hbar^3*sqrt(energyDifference);
    end
end

function f = calc_f(from,to,n,props)
    % Calculate the Fermi-Dirac occupation probability at the energy of an
    % electron from trap "from" in trap "to"
    
    q = 1.602e-19;          % Elemental charge
    k = 1.381e-23;          % Boltzmann constant
    
    if from.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
    end
    
    if to.isIL
        E_F = props.E_F_il * props.phi_g0_il * q;
    else
        E_F = props.E_F_ox * props.phi_g0_ox * q;
    end
    
    f = 1/(1 + exp(((-from.phi_ex*q - from.E + q) + n*hbarOmega0 - (-to.phi_ex*q - E_F))/(k*to.T)));

%     fprintf('%.3f - %.3f --> %.8f\n',(-from.phi_ex*q - from.E + q + n*hbarOmega0)/q,(-to.phi_ex*q - E_F)/q,f)
end

function [Ca, Em] = calc_CaEm(trap,n,props)
    % Calculate capture and emission rates of trap "trap" given n phonons
    
    q = 1.602e-19;              % Elemental charge
    hbar = 1.055e-34;           % Modified Planck constant
    m_e = props.m_e*9.110e-31;  % Electron effective mass
    k = 1.381e-23;              % Boltzmann constant
    S = props.S;                % Huang-Rhys factor
    
    if trap.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
        phi_g0 = props.phi_g0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
        phi_g0 = props.phi_g0_ox * q;
    end
    
    % For electrodes, trap radius is actually 0, but this will only reflect 
    % in the tunneling probability. Since in Vandelli/Larcher c_0 is 
    % assumed a constant, R_T for electrodes is set the same value as traps
    if trap.isIL; R_T = sqrt(1e-18/pi); else; R_T = trap.R_T; end
    
    % Constant dependent on field
    c_0 = ((4*pi)^2)*(R_T^3)*(q^2)*(trap.F_ex^2)*hbar ... 
        / (phi_g0 * 2*m_e);
    
    % Bose function
    f_B = 1/(exp(hbarOmega0/(k*trap.T)) - 1);
    
    % Bessel function
    I_n = besseli(n, 2*S*sqrt(f_B*(f_B+1)));
    
    % Multiphonon transition probability
    L = ((f_B + 1)/f_B)^(n/2)*exp(-S*(2*f_B+1))*I_n;
    
    % Capture rate
    Ca = c_0*L;
    
    % Emission rate
    Em = Ca*exp(-n*hbarOmega0/(k*trap.T));
end
