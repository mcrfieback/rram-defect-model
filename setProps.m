function setProps(varargin)
    % SETPROPS Sets the model properties in a global variable PROPS.
    % Accepts field-value pairs to modify properties from the default
    % value.
    
    if mod(nargin,2) == 1; error('Incomplete field-value pairs.'); end

    global PROPS;
    
    PROPS = struct( ...
    ... General properties
    'epsilon', 21, ...          [-]    Relative dielectric constant of HfO2
    'k_th', 0.5, ...            [K/Wm] Thermal conductivity of HfO2
    ...
    ... Build Lattice properties
    'L_step', 3, ...            [�]    Lattice spacing step
    'E_C_el', 4.45, ...         [eV]   Electrode conduction band energy (TODO: diff. bet. E_C HfO2 and E_C TiN?)
    'E_C_il', 2.8, ...          [eV]   IL conduction band energy (TODO: diff. bet. E_C IL and E_C HfO2?)
    'E_Tmu', 1.9, ...           [eV]   Trap energy average
    'E_Tdelta', 0.5, ...        [eV]   Trap energy deviation
    'R_T', 5.64e-10, ...        [m]    Trap radius
    ...
    ... TAT properties
    'm_e', 0.18, ...            [-]    Relative effective electron mass in HfO2
    'hbarOmega0_ox', 0.07, ...  [eV]   Effective phonon energy in HfO2
    'hbarOmega0_il', 0.06, ...  [eV]   Effective phonon energy in IL
    'phi_g0_el', 0, ...         [eV]   Band gap energy of electrode
    'phi_g0_ox', 5.8, ...       [eV]   Band gap energy of HfO2
    'phi_g0_il', 8.9, ...       [eV]   Band gap energy of IL
    'E_F_el', 0, ...            [-]    Location of Fermi energy in gap from conduction band of TiN
    'E_F_ox', 1/2, ...          [-]    Location of Fermi energy in gap from conduction band of HfO2
    'E_F_il', 1/4, ...          [-]    Location of Fermi energy in gap from conduction band of IL
    'S', 17, ...                [-]    Huang-Rhys factor
    ...
    ... kMC properties
    'E_A', 1.68, ...            [eV]   O-Hf bond breakage zero-field effective activation energy
    'E_AD_v', 1.5, ...          [eV]   Diffusion activation energy (vacancy)
    'E_AD_i' , 0.7, ...         [eV]   Diffusion activation energy (ion)
    'E_AR', 0.2, ...            [eV]   Recombination activation energy
    'p_0', 5.2, ...             [e�]   O-Hf dipole moment
    'nu', 7e13, ...             [Hz]   Effective vibration frequency of O-Hf bonds
    'k_D', 30 ...               [e�]   Factor dependent on molecular properties of HfO2
    );

    for n = 1:2:numel(varargin)-1
        PROPS.(varargin{n}) = varargin{n+1};
    end
end