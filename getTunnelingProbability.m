function P_T = getTunnelingProbability(trap_i, trap_j, E, m_e)
    % GETTUNNELINGPROBABILITY Calculates tunneling probability P_T from
    % trap i to trap j for a particle with energy E and effective mass m_e.
    
    % Physical constants
    q = 1.602e-19;              % Elemental charge
    hbar = 1.055e-34;           % Modified Planck constant
    
    if ~(trap_i.is('V+2') ...
    ||  (trap_i.is('OEL') && trap_i.isIL) ...
    ||  (trap_i.is('lower') && trap_i.isIL)) ...
    || ~(trap_j.is('V+2') ...
    ||  (trap_j.is('OEL') && trap_j.isIL) ...
    ||  (trap_j.is('lower') && trap_j.isIL))
        warning(['One of the grid points is not an IL or a trap: results can be unexpected. (' ...
            trap_i.type '[' num2str(trap_i.isIL) '] and ' trap_j.type '[' num2str(trap_j.isIL) '])']);
    end
    
    % Potential barrier bounds
    U_i = trap_i.E;                                % Effective barrier at trap i is its energy
    U_j = U_i - (trap_j.phi_ex - trap_i.phi_ex)*q; % Effective barrier at trap j is U_i minus the potential difference
    
    if (E > U_i) && (E > U_j)
        % CASE 1
        % There is no potential barrier.
        P_T = 1;
        return;
    end
    
    % Get (precalc) shortest distance between trap and electrode (if elec.)
    if trap_i.is('upper')
        % Trap i is the upper electrode
        D_ij = trap_j.d_U;
    elseif trap_j.is('upper')
        % Trap j is the upper electrode
        D_ij = trap_i.d_U;
    elseif trap_i.is('lower')
        % Trap i is the lower electrode
        D_ij = trap_j.d_L;
    elseif trap_j.is('lower')
        % Trap j is the lower electrode
        D_ij = trap_i.d_L;
    else
        % Calculate distance between trap i and trap j
        D_ij = sqrt((trap_j.x - trap_i.x)^2 ...
                + (trap_j.y - trap_i.y)^2 ...
                + (trap_j.z - trap_i.z)^2);
    end
    
    % Trap j energy
    E_Tj = trap_j.E;
    
    % Force gradient
    F_ij = (U_i - U_j)/D_ij;
    
    % Capture radii of traps and potential value at radii
    R_i = trap_i.R_T;
    R_j = trap_j.R_T;
    U_Ri = U_i - F_ij*R_i;
    U_Rj = U_j + F_ij*R_j;
    
    if (U_Ri <= E) || (U_Ri/R_i*D_ij <= E)
        % CASE 1
        % F_ij is too steep, there is no more potential barrier.
        P_T = 1;
    elseif U_Rj < E
        % CASE 2
        % Two-piece linear: 
        % 1. trap i radius
        % 2. potential gradient
        
        P_T = exp(4/3*sqrt(2*m_e)/(hbar) * ...
            (R_i/U_Ri*(-(U_Ri - E)^(3/2)) ...
           + 1/F_ij*(-(U_Ri - E)^(3/2))) ...
           );
       
    elseif D_ij < (R_i + R_j)
        if R_i == 0
            if U_j - E_Tj + (E_Tj/R_j + F_ij)*D_ij < E
                % CASE 1
                % The left side peak of the barrier is under E and has no effect.
                
                P_T = 1;
            else
                % CASE 3.1
                % One-piece linear:
                % 1. trap j radius

                P_T = exp(4/3*sqrt(2*m_e)/(hbar) * ...
                (1/(E_Tj/R_j + F_ij)*(-(U_j - E_Tj + (E_Tj/R_j + F_ij)*D_ij - E)^(3/2))) ...
               );
            end
        elseif R_j == 0
            if (U_i/R_i - F_ij)*D_ij < E
                % CASE 1
                % The right side peak of the barrier is under E and has no effect.
                
                P_T = 1;
            else
                % CASE 3.2
                % One-piece linear:
                % 1. trap i radius

                P_T = exp(4/3*sqrt(2*m_e)/(hbar) * ...
                    (R_i/U_Ri*(-((U_i/R_i - F_ij)*D_ij - E)^(3/2))) ...
                   );
            end
        else
            xcross = (U_j - E_Tj + (E_Tj/R_j + F_ij)*D_ij) ... 
                   / (U_i/R_i + E_Tj/R_j);
               
            if U_Ri/R_i*xcross < E
                % CASE 1
                % The middle peak of the barrier is under E and has no effect.
                
                P_T = 1;
            else
                % CASE 3.3
                % Two-piece linear: 
                % 1. trap i radius
                % 2. trap j radius

                P_T = exp(4/3*sqrt(2*m_e)/(hbar) * ...
                    (R_i/U_Ri*(-(U_Ri/R_i*xcross - E)^(3/2)) ...
                   + 1/(E_Tj/R_j + F_ij)*(-(U_j - E_Tj + (E_Tj/R_j + F_ij)*(D_ij - xcross) - E)^(3/2))) ...
                   );
            end
        end
    else
        % CASE 4
        % Three-piece linear: 
        % 1. trap i radius
        % 2. potential gradient
        % 3. trap j radius
        
        if F_ij == 0
            P_T = exp(4/3*sqrt(2*m_e)/(hbar) * ...
                (R_i/U_Ri*(-(U_Ri - E)^(3/2)) ...
               + (U_i - E)^(1/2)*-(D_ij - R_i - R_j) ...
               + R_j/E_Tj*(-(U_Rj - E)^(3/2))) ...
               );
        else
            P_T = exp(4/3*sqrt(2*m_e)/(hbar) * ...
                (R_i/U_Ri*(-(U_Ri - E)^(3/2)) ...
               + 1/F_ij*((U_Rj - E)^(3/2)-(U_Ri - E)^(3/2)) ...
               + 1/(E_Tj/R_j + F_ij)*(-(U_Rj - E)^(3/2))) ...
               );
        end
    end
end
