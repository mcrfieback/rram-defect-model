function grid = buildReducedLatticeGrid(lat_props,V_U,V_L,T_amb)
    %BUILDREDUCEDLATTICEGRID Build a "grid" with only traps and electrodes
    %   This function can be used to speed up generation and handling of a 
    %   very large RRAM stack but can only be used to calculate current, so
    %   to test the TAT solver.
    %
    %   DO NOT USE FOR THE FULL MODEL!!
    
    debugDisp('Building reduced lattice...');
    
    global PROPS;
    q = 1.602e-19;                % [C] Elemental charge

    E_C_il   = PROPS.E_C_il*q;    % [J] IL conduction band energy
    E_Tmu    = PROPS.E_Tmu*q;     % [J] Average trap energy
    E_Tdelta = PROPS.E_Tdelta*q;  % [J] Trap energy deviation
    R_T      = PROPS.R_T;         % [m] Trap capture radius
    
    d_x      = lat_props.d_x;     % [-] Length in X direction
    d_y      = lat_props.d_y;     % [-] Length in Y direction
    d_z      = lat_props.d_z;     % [-] Length in Z direction
    
    grid_type = lat_props.grid_type;
    
    % Preallocate grid
    debugDisp('Preallocating grid...');
    % Predict the amount of traps
    if strcmp(grid_type, 'single trap')
        N_traps = 1;
    else
        N_traps = round(lat_props.vacancyDistribution*1e6*(d_x*d_y*d_z));
        if N_traps == 0
            warning('Generating 0 traps: vacancy density too low.');
        else
            debugDisp(['Generating ' num2str(N_traps) ' traps.']);
        end
    end
    
    % Include a row for upper and lower electrode points to work with the TATSolver
    N_traps = N_traps + 1;
    
    % "Grid" is now ordered as three rows: upper; trap; lower
    grid((1:N_traps),3) = lattice_element();
    
    F = (V_U-V_L)/d_z;
    
    for n = 1:N_traps-1
        % Trap
        tr = grid(n,2);
        
        tr.index = sub2ind([N_traps 3],n,2);
        tr.T = T_amb;
        
        if strcmp(grid_type, 'single trap')
            tr.x = d_x/2;
            tr.y = d_y/2;
            tr.z = d_z/2;
        else
            tr.x = rand*d_x;
            tr.y = rand*d_y;
            tr.z = rand*d_z;
        end
        
        tr.type = 'V+2'; % vacancy
        tr.E = E_Tmu + (2*rand-1)*E_Tdelta; % trap energy
        tr.phi_ex = V_L+(tr.z*F);
        tr.F_ex = F;
        tr.R_T = R_T; % trap capture radius
        
        tr.index_U = sub2ind([N_traps 3],n,1);
        tr.index_L = sub2ind([N_traps 3],n,3);
        tr.d_U = d_z - tr.z;
        tr.d_L = tr.z;

        grid(n,2) = tr;
        
        % Upper electrode
        up = grid(n,1);
        
        up.index = sub2ind([N_traps 3],n,1);
        up.T = T_amb;
        
        up.x = tr.x;
        up.y = tr.y;
        up.z = d_z;
        
        up.type = 'OEL'; % upper IL
        up.isIL = 1;
        up.phi_ex = V_U;
        up.F_ex = F;
        up.E = E_C_il; % conduction band energy
        
        up.index_L = sub2ind([N_traps 3],n,3);
        up.d_L = d_z;
        
        grid(n,1) = up;
        
        % Lower electrode
        lo = grid(n,3);
        
        lo.index = sub2ind([N_traps 3],n,3);
        lo.T = T_amb;
        
        lo.x = tr.x;
        lo.y = tr.y;
        lo.z = 0;
        
        lo.type = 'lower'; % lower IL
        lo.isIL = 1;
        lo.phi_ex = V_L;
        lo.F_ex = F;
        lo.E = E_C_il; % conduction band energy
        
        lo.index_U = sub2ind([N_traps 3],n,1);
        lo.d_U = d_z;
        
        grid(n,3) = lo;
    end
    
    % Do this to make it work with the TAT solver (looks for non-ILs for voltages)
    up.type = 'upper';
    up.isIL = 0;
    grid(N_traps,1:2) = [up; up];
    lo.type = 'lower';
    lo.isIL = 0;
    grid(N_traps,3) = lo;
    
    debugDisp('Build Reduced Lattice Grid done.');
end

function debugDisp(text)
    if checkIfDebugging('buildReducedLatticeGrid')
        disp(text);
    end
end