function plotBlockData2(x,y,z,vs,gs,vrange)
    % PLOTBLOCKDATA2 Improved version of plotBlockData that accepts
    % non-uniform coordinates and a custom color range.

    vs = vs(:);
    
    if any(isinf(vs) | isnan(vs))
        errorIndices = find(isinf(vs) | isnan(vs));
%         warning('Found %d values that are NaN or infinite.',length(errorIndices));
        vs(errorIndices) = min(vs);
    else
        errorIndices = [];
    end
    
    if ~exist('vrange','var')
        vrange = [];
        vr = max(vs)-min(vs); 
    else
        vr = vrange(2)-vrange(1);
    end
    if vr == 0; vr = 1; end

    N_x = gs(1); N_y = gs(2); N_z = gs(3);
    H_x = round(N_x/2); H_y = round(N_y/2);
    
    ix1 = repmat(N_x+N_x*(0:N_y-1)', [1 N_z]) + N_y*N_x*(cumsum(ones(N_y,N_z),2)-1);
    ix2 = repmat(H_x+1+N_x*(0:H_y)', [1 N_z]) + N_y*N_x*(cumsum(ones(H_y+1,N_z),2)-1);
    ix3 = repmat(1+N_x*(H_y:N_y-1)', [1 N_z]) + N_y*N_x*(cumsum(ones(N_y-H_y,N_z),2)-1);

    iy1 = repmat((1:(N_x))'+N_x*(N_y-1), [1 N_z]) + N_y*N_x*(cumsum(ones(N_x,N_z),2)-1);
    iy2 = repmat((1:(H_x+1))'+N_x*H_y, [1 N_z]) + N_y*N_x*(cumsum(ones(H_x+1,N_z),2)-1);
    iy3 = repmat((H_x+1:N_x)', [1 N_z]) + N_y*N_x*(cumsum(ones(N_x-H_x,N_z),2)-1);


    iz1 = repmat((H_x+1:N_x)', [1 H_y+1]) + N_x*(cumsum(ones(N_x-H_x,H_y+1),2)-1);
    iz2 = repmat((1:N_x)', [1 N_y-H_y]) + N_x*(cumsum(ones(N_x,N_y-H_y),2)+H_y-1);
    iz3 = iz1 + (N_z-1)*N_y*N_x;
    iz4 = iz2 + (N_z-1)*N_y*N_x;

    planeIndices = {ix1; ix2; ix3; iy1; iy2; iy3; iz1; iz2; iz3; iz4};
    clear ix1 ix2 ix3 iy1 iy2 iy3 iz1 iz2 iz3 iz4;

    cmap = colormap;

    vn = round(255*(vs - min(vs))/vr)+1;
    vn(vn > 256) = 256; vn(vn < 1) = 1;
    

    for n = 1:length(planeIndices)
        pi = planeIndices{n};
        C = zeros([size(pi) 3]);
        for ii = 1:size(C,1)
            for jj = 1:size(C,2)
                if any(errorIndices == pi(ii,jj))
                    C(ii,jj,:) = [1 1 1];
                else
                    C(ii,jj,:) = cmap(vn(pi(ii,jj)),:);
                end
            end
        end
        surface(x(pi),y(pi),z(pi),C,'EdgeColor',cmap(1,:),'FaceColor','interp');
    end
    clear pi;
    
    if ~isempty(vrange) && (vrange(1) < vrange(2))
        caxis(vrange);
    elseif max(vs) ~= min(vs)
        caxis([min(vs) max(vs)]); 
    else
        caxis([vs(1)-1 vs(1)+1])
    end
    
    colorbar;
    grid;
    view(-45,20);
end