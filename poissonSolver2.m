function phi = poissonSolver2(A, phi, b)
% POISSONSOLVER Solves a three-dimensional poisson equation, given
% arbitrary coordinates, from field fieldFrom to field fieldTo. Grid types
% that should act as boundaries are specified in boundTypes.

    % Use Conjugate Gradient method to solve linear system of equations

    maxIter = 5000;
    precisionGoal = 1e-25;
    

    r = b-smvp(A,phi);
    r2 = 1;

    for run = 1:maxIter
        r1 = r'*r;
        if r1 < precisionGoal
            break;
        end
        if run == 1
            p = r; 
        else
            p = r + (r1/r2)*p;
        end
        
        % q = A*p;
        q = smvp(A,p);
        a = r1/(p'*q);
        if isnan(a) || isinf(a)
            error('CG divergence.');
        end

        phi = phi + a*p;
        r = r - a*q;

        r2 = r1;
    end
end

function v = smvp(A,x)
    % Do a Sparse Matrix Vector Product with sparsified A rows
    v = zeros(length(x),1);
    for ii = 1:length(A)
        A_row = A{ii};
        for n = 1:size(A_row,1)
            v(ii) = v(ii) + A_row(n,2)*x(A_row(n,1));
        end
    end
end
