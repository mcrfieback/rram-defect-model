function collectTATSweep(nameFormat,N,N_s,outName)
    % COLLECTTATSWEEP Collects the most important data from a TAT sweep.
    % Accepts a nameFormat with %d for run number, collects N runs and
    % saves them in outName.
    
    J = zeros(N,N_s);
    
    for n = 1:N_s
        padZeros = repelem('0',floor(log10(N_s)) - floor(log10(n)));
        fname = sprintf(nameFormat,padZeros,n);
        if ~exist(fname,'file'); continue; end
        d = load(fname,'J');
        J(:,n) = d.J;
    end
    
    save(outName,'J');
end
