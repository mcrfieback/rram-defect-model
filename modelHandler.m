function device = modelHandler(varargin)
    % MODELHANDLER Overview function that handles the MATLAB RRAM model.
    % Takes a filename for the output file and optional field-value pairs to
    % set model parameters. Unset fields use the default value.

    %% Parse arguments

    if mod(nargin,2) == 0
        fname = ['model_out_' datestr(datetime,'yyyymmddhhMMss') '.mat'];
        args = cell2struct(varargin(2:2:end),varargin(1:2:end-1),2);
    else
        fname = varargin{1};
        args = cell2struct(varargin(3:2:end),varargin(2:2:end-1),2);
    end
    
    if isempty(regexp(fname,'.+(\.mat)','ONCE'))
        error('Invalid filename: ''%s''', fname);
    end

    %% Set default structure

    args_default = struct( ...
        ... Simulation parameters
        'doPlot', 1, ...
        'doExit', 0, ...
        'saveDevice', 0, ...
        'maxSteps', 2000, ...
        'rngSeed', [], ...
        ... Model parameters
        't_max', 5e-6, ...      [s] Max simulation time limit
        'dV_max', 0.1, ...      [V] Max change in voltage per step
        'I_comp', 1e-6, ...     [A] Compliance current
        'T_amb', 300, ...       [K] Ambient temperature
        'V', '3.0*t/t_max', ... [V] Symbolic function of t (as string)
        ... Lattice parameters (exist in lat_props)
        'N_x', 30, ...
        'N_y', 30, ...
        'N_z', 40, ...
        'grid_type', 'regular+GB', ...
        'vacancyDistribution', 3.0e19, ... 
        'GBDistribution', 2.1e21, ... 
        'electrodeRoughness', 0, ...
        'impurityType', 'none', ...
        'impurityWidth', [], ...
        'impurityHeight', [], ...
        'impurityPos', [] ...
        );

    %% Initialize constants

    atLatticeParameters = false;
    
    f = fields(args_default);
    N = length(f);
    
    removeFields = {};
    
    % Evaluate parameters from supplied arguments or default
    for n = 1:N
        if strcmp(f{n},'N_x')
            % Reached the lattice parameters, put remainder in lat_props
            atLatticeParameters = true;
            cmd = 'lat_props = struct(';
        end
        
        argSupplied = isfield(args,f{n});
        
        if atLatticeParameters
            % Build lat_props struct in a command line
            if argSupplied
                cmd = sprintf('%s''%s'', args.%s', cmd, f{n} ,f{n});
                removeFields = [removeFields f{n}]; % Set argument to be removed
            else
                cmd = sprintf('%s''%s'', args_default.%s', cmd, f{n} ,f{n});
            end
            if n == N
                % Evaluate the command
                eval([cmd ');']); break;
            else
                % Add a comma and continue
                cmd = sprintf('%s,',cmd); continue;
            end
        else
            % Directly evaluate parameters
            if argSupplied
                eval(sprintf('%s = args.%s;', f{n}, f{n}));
                removeFields = [removeFields f{n}]; % Set argument to be removed
            else
                eval(sprintf('%s = args_default.%s;', f{n}, f{n}));
            end
        end
    end
    
    % Remove supplied arguments fields of model parameters
    args = rmfield(args,removeFields);
    
    % Use the remaining arguments to set the model properties in setProps
    f = fieldnames(args);
    c = struct2cell(args);
    argsCell = flipud([c f]');
    setProps(argsCell{:});

    %% Initialize variables

    % Set outputs
    doDebug('doEvent');

    % Set logging variables
    tvec = zeros(0,1);  % Time logging vector
    Ivec = zeros(0,3);  % Current logging vector
%     evec = zeros(0,1);  % Event logging vector
    Vvec = zeros(0,1);  % Voltage logging vector
    
    % Lattice grid logging structure vector
%     gridvec = struct('type',{},'x',{},'y',{},'z',{},'T',{},'Q',{});

    % Initialize current
    I = 0;

    % Build symbolic voltage function V(t) from supplied string function
    syms t dt_limit;
    assume(dt_limit > 0);
    eval(['V = ' V ';']);
    
    % Set random seed if specified
    if ~isempty(rngSeed)
        rng(rngSeed);
    end
    
    t = 0;

    %% Build model

    device = model(eval(subs(V)),0,T_amb,lat_props);

    %% Step until max time (or compliance current) is reached

    step = 1;

    while step < maxSteps && t < t_max && I(1) < I_comp
        disp(['-- [Step ' num2str(step) ']: t = ' num2str(t) 's; V = ' num2str(eval(subs(V))) 'V']);
        
        % Find the max timestep given the max voltage step and V(t)
        dV = abs(subs(V,t+dt_limit) - subs(V));
        dt_limitSolution = eval(solve(dV == dV_max, dt_limit));

        if isempty(dt_limitSolution)
            % If V(t) doesn't limit dt, the simulation time does
            dt_limitSolution = t_max - t;
        end

        % Update the applied voltage
        device.updateVoltage(eval(subs(V)),0);

        % Do a kMC step
        [dt,I,~] = device.doStep(dt_limitSolution);

        % Log the results
        tvec(step) = t*1e9;
        Ivec(step,:) = I;
%         evec(step) = e;
        Vvec(step) = eval(subs(V));
%         gridvec = [gridvec; struct( ...
%             'type',device.grid.getTypeNumber, ...
%             'x',[device.grid.x], ...
%             'y',[device.grid.y], ...
%             'z',[device.grid.z], ...
%             'T',[device.grid.T], ...
%             'Q',[device.grid.Q]) ...
%             ];

        if doPlot
            % Plot the status of the grid
%             subplot(2,3,[1 4]);
%                 device.plotGrid;
%                 title(['t = ' num2str(t) ' s']);
% 
%             subplot(2,3,[2 3]);
                semilogy(tvec,Ivec,'k.-')
%                          tvec(step),Ivec(step),'mo', ... This step
%                          tvec(evec == 1),Ivec(evec == 1),'g+', ... Generation events
%                          tvec(evec == 2),Ivec(evec == 2),'b^', ... Ion drift events
%                          tvec(evec == 3),Ivec(evec == 3),'rx'  ... Recombination events
%                          );
                xlabel('t [ns]')
                ylabel('I [A]')
                title('Current vs. time');

%             subplot(235);
%                 device.plotGrid('T');
%                 title('Temperature');

%             subplot(236);
%                 device.plotGrid('F');
%                 title('Local field magnitude');

            drawnow;
        end

        disp(['I = ' num2str(I) ' A'])
        
        % Save progress every 100 steps
%         if mod(step,100) == 0
%             disp('Saving model output...');
%             save(fname,'gs','gridvec','tvec','Vvec','Ivec','evec');
%         end

        % Go to the next step
        t = t + dt;
        step = step + 1;
    end

    %% Save model output

    disp('Saving model output...');
    if saveDevice
        save(fname,'tvec','Vvec','Ivec','device');
    else
        save(fname,'tvec','Vvec','Ivec');
    end
%     save(fname,'gs','gridvec','tvec','Vvec','Ivec','evec');

    if doPlot
        % Plot final results
        figure;
        plotModelOutput(fname,1e-6);
    end
    
    if doExit; exit; end
end
