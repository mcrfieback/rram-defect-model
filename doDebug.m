function doDebug(f)
    % DODEBUG Turns the displaying of debug messages on or off for func f.
    % Use a string as argument to activate function debug.
    global DEBUG_MODE;
    DEBUG_MODE.on = true;
    if length(fields(DEBUG_MODE)) == 1
        DEBUG_MODE.do = {f};
    else
        DEBUG_MODE.do = [DEBUG_MODE.do {f}];
    end
end

