function [t,V,I,grids,gridsInt] = plotModelOutput_old(varargin)
    % PLOTMODELOUTPUT plots the results in model_out.
    % Reads out and plots voltage, current and grid configurations at the
    % timestamps given.
    
    switch nargin
        case 0
            fname = 'model_out.txt';
            doPlot = true;
            minStep = 0;
        case 1
            fname = varargin{1};
            doPlot = true;
            minStep = 0;
        case 2
            fname = varargin{1};
            doPlot = varargin{2};
            minStep = 0;
        case 3
            fname = varargin{1};
            doPlot = varargin{2};
            minStep = varargin{3};
        otherwise
            error('Too many arguments supplied.');
    end
    
    %% Read values
    f = fopen(fname);
    
    % Get grid size
    gs = fscanf(f,'grid size: %d %d %d\nt V I event grid\n')';
    
    t = zeros(0,1);
    V = zeros(0,1);
    I = zeros(0,1);
    e = zeros(0,1);
    grids = cell(0,1);
    gridsInt = cell(0,1);
    
    N = 0;
    while (1)
        C = fscanf(f,'%f',4);
        if isempty(C); break; end
        N = N + 1;
        t = [t; C(1)]; V = [V; C(2)]; I = [I; C(3)]; e = [e; C(4)];
        
        % Get grid array
        ga = fscanf(f,'%d;%d');
        
        gr = zeros(gs); % Grid type ids
        gi = 0;         % Grid index
        % Build grid ids from array
        for nn = 1:2:length(ga)-1
            gr((gi+1):(gi+ga(nn+1))) = ga(nn);
            gi = gi + ga(nn+1);
        end
        
        grids = [grids; gr];
        
        fscanf(f,';;');
        % Get interstitial grid array
        ga = fscanf(f,'%d[%d]');
        if isempty(ga)
            gridsInt = [gridsInt; 'none'];
        else
            gr = zeros(length(ga)/2,4); % Interstitial grid coordinates
            % Build interstitial coords from array
            for nn = 1:size(gr,1)
                gr(nn,1) = ga(2*nn-1);
                [gr(nn,2), gr(nn,3), gr(nn,4)] = ind2sub(gs,ga(2*nn));
                gr(nn,2:4) = gr(nn,2:4)+0.5;
            end
            gridsInt = [gridsInt; gr];
        end
        
        fscanf(f,';;\n');
    end
    
    fclose(f);
    
    if ~doPlot; return; end
    
    %% Plot values
    
    x = repmat(1:gs(1),[1 gs(2)*gs(3)])';
    y = repmat(repelem(1:gs(2),gs(1)),[1 gs(3)])';
    z = repelem(1:gs(3),gs(2)*gs(1))';
    
    inStep = 0;
    
    tLast = 0;
    
    for n = 1:N
        inStep = inStep + t(n) - tLast;
        tLast = t(n);
        if inStep < minStep && ~(n == N)
            continue;
        else
            inStep = 0;
        end
        subplot(221)
            semilogy(t,I,'k.:',t(1:n),I(1:n),'k.-', ...
                     t(e == 1),I(e == 1),'g+', ...
                     t(e == 2),I(e == 2),'b^', ...
                     t(e == 3),I(e == 3),'rx');
            title('Current vs. time');
            xlabel('t [ns]');
            ylabel('I [A]');
        subplot(222)
            plot(t,V,'k.:',t(1:n),V(1:n),'k.-');
            title('Voltage vs. time');
            xlabel('t [ns]');
            ylabel('V [V]');
        subplot(223)
            semilogy(V,I,'k.:',V(1:n),I(1:n),'k.-', ...
                     V(e == 1),I(e == 1),'g+', ...
                     V(e == 2),I(e == 2),'b^', ...
                     V(e == 3),I(e == 3),'rx');
            title('Current vs. voltage');
            xlabel('V [V]');
            ylabel('I [A]');
        subplot(224)
            % Plot the grid at timestamp
            gr = grids{n};
            % lower
            plot3(x(gr == 1),y(gr == 1),z(gr == 1),'k^', ...
                'MarkerSize',10,'MarkerFaceColor',[0.4 0.4 0.4])
            hold on;
%             % upper
%             plot3(x(gr == 2),y(gr == 2),z(gr == 2),'kv', ...
%                 'MarkerSize',10,'MarkerFaceColor','none');
%             % OEL
%             plot3(x(gr == 3),y(gr == 3),z(gr == 3),'ko', ...
%                 'MarkerSize',3,'MarkerFaceColor',[0.6 0.6 0.6]);
            % V+2
            plot3(x(gr == 6),y(gr == 6),z(gr == 6),'ko', ...
                'MarkerSize',10,'MarkerFaceColor','b');
            % Plot the interstitials at timestamp
            gri = gridsInt{n};
            if ~strcmp(gri,'none')
                % OEL_ox
                plot3(gri(gri(:,1) == 4,2),gri(gri(:,1) == 4,3),gri(gri(:,1) == 4,4),'ko', ...
                    'MarkerSize',10,'MarkerFaceColor',[1 1 1]);
                % O-2
                plot3(gri(gri(:,1) == 5,2),gri(gri(:,1) == 5,3),gri(gri(:,1) == 5,4),'ko', ...
                    'MarkerSize',10,'MarkerFaceColor','r');
            end
            hold off;
            
            % Build legend
%             typeNames = {'oxide','lower','upper','OEL','OEL_ox','O-2','V+2'};
%             legendLabels = {};
%             for nn = 2:length(typeNames)
%                 if any(gr(:) == nn-1) || any(gri(:,1) == nn-1)
%                     legendLabels = [legendLabels typeNames{nn}];
%                 end
%             end
            
            xlabel('x');
            ylabel('y');
            zlabel('z');
            title(['Grid at t = ' num2str(t(n),'%10.4es')]);
%             legend(legendLabels);
            view(50,10);
        drawnow;
    end
    
end