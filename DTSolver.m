function [I,P] = DTSolver(grid)
    % DTSOLVER Calculates the direct current through a grid of
    % lattice_elements from the upper to lower electrode, using the
    % Tsu-Esaki formula.
    
    global PROPS;
    props = PROPS;
    
    P = zeros(size(grid));              % [W] Dissipated power in grid
    
    % Get the upper and lower electrode potentials
    V_U = grid(find(grid.is('upper'), 1)).phi;
    V_L = grid(find(grid.is('lower'), 1)).phi;
    
    % Check which electrode is the anode
    isLowerAnode = V_U < V_L;
    
    V = abs(V_U - V_L);
    
    if V == 0; I = 0; return; end

    % Electrons tunnel from the cathode (-) to the anode (+)
    if isLowerAnode
        % Cathode interface layer is on OEL
        ils = grid([grid.isIL] & grid.is('OEL'));
    else
        % Cathode interface layer is on lower
        ils = grid([grid.isIL] & grid.is('lower'));
    end
    
    for n = 1:numel(ils)
        if isLowerAnode
            % Oxide thickness is distance OEL to lower
            t_ox = ils(n).d_L;
            index_an = ils(n).index_L;
        else
            % Oxide thickness is distance lower to OEL
            t_ox = ils(n).d_U;
            index_an = ils(n).index_U;
        end
        T = ils(n).T;
        J = getJ(V,T,t_ox,props);
        P(index_an) = sum(J)*props.L_step^2*1e-20*V;
    end
    
    I = sum(J * props.L_step^2*1e-20);
end

function J = getJ(V,T,t_ox,props)
    
    q = 1.602e-19;              % Elemental charge
    h = 6.626e-34;              % Planck constant
    m_e = props.m_e*9.110e-31;  % Electron effective mass
    k = 1.381e-23;              % Boltzmann constant
    
    % For the next energy levels, Fermi level at cathode is considered 0.
    Emin = [
        props.E_F_el*props.phi_g0_el*q;      % ECB: conduction band of cathode
        (1-props.E_F_el)*props.phi_g0_el*q;  % HVB: valence band of anode
        (props.E_F_el*props.phi_g0_el-V)*q;  % EVB: conduction band of anode
        ];
    Emax = [
        props.E_F_ox*props.phi_g0_ox*q;      % ECB: conduction band of dielectric
        (1-props.E_F_ox)*props.phi_g0_ox*q;  % HVB: valence band of dielectric
        -(1-props.E_F_el)*props.phi_g0_el*q; % EVB: valence band of cathode
        ];

    % Fermi level at anode (rel. to cathode) (should be < 0)
    E_Fa = -q*(V);
    
    % Height of left side of potential wall
    U_i = [
        Emax(1);        % ECB: conduction band of dielectric at cathode
        Emax(2);        % HVB: valence band of dielectric at anode
        Emax(1);        % EVB: conduction band of dielectric at cathode
        ];
    % Height of right side of potential wall
    U_j = [
        E_Fa+Emax(1);   % ECB: conduction band of dielectric at anode
        E_Fa+Emax(2);   % HVB: valence band of dielectric at cathode
        E_Fa+Emax(1);   % EVB: conduction band of dielectric at anode
        ];
    
    % If Emin > Emax for HVB, it means the anode valence band is under the
    % dielectric valence band. Therefore there is no potential barrier but
    % still a supply of holes so the integration limits must be swapped.
    if Emin(2) > Emax(2)
        tmp = Emin(2);
        Emin(2) = Emax(2);
        Emax(2) = tmp;
    end
    
    % Resolution of numeric integration
    N_E = 200;
    dE = (Emax-Emin)/(N_E-1);
    
    J = [0;0;0];
    for n = 1:3
        E_ilast = 0;
        J(n) = 0;
        for E = Emin(n):dE:Emax(n)
            NEx = k*T*log((1 + exp(-(E)/(k*T)))/(1 + exp(-(E - E_Fa)/(k*T))));
            if NEx == 0
                % All NEx will be 0 from now so continuing is unnecessary
                break;
            end
            TC = getWKB(U_i(n),U_j(n),t_ox,E,m_e);
            E_i = (TC*NEx);
            J(n) = J(n) + dE(n)*(E_ilast+E_i)/2;
            E_ilast = E_i;
        end
        J(n) = 4*pi*q*m_e/(h^3) * J(n);
    end
end

function TC = getWKB(U_i,U_j,D_ij,E,m_e)
    hbar = 1.055e-34;           % Modified Planck constant
    
    F_ij = (U_i-U_j)/D_ij;
    
    if (E > U_i) && (E > U_j)
        TC = 1;
    elseif E > U_j
        TC = exp(4/3*sqrt(2*m_e)/(hbar) * ...
               1/F_ij*(-(U_i - E)^(3/2)) ...
               );
    else
        if F_ij == 0
            TC = exp(4/3*sqrt(2*m_e)/(hbar) * ...
               -(U_i - E)^(1/2)*(D_ij) ...
               );
        else
            TC = exp(4/3*sqrt(2*m_e)/(hbar) * ...
               1/F_ij*((U_j - E)^(3/2)-(U_i - E)^(3/2)) ...
               );
        end
    end
end