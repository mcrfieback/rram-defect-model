function plotSweep(fname)
    %
    load(fname,'t','I','V','e');
    
    N = numel(t);
    
    Imax = zeros(N,1);
    somethingHappened = false(N,1);
    for n = 1:N
        if ~isempty(I{n}); Imax(n) = max(I{n}); end
        if any(e{n}); somethingHappened(n) = true; end
    end
    
    disp(nnz(somethingHappened));
    
    Ilimit = 1e-7;
    
    clf;
    semilogy(t{1},I{1});
    hold on;
    for n = 2:N
        if somethingHappened(n)
            c = 'b.-';
        else
            c = 'r.-';
        end
        semilogy(t{n},I{n},c);
    end
    xlabel('t [ns] \rightarrow');
    ylabel('I [A] \rightarrow');
    hold off;
end