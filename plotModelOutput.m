function plotModelOutput(varargin)
    % PLOTMODELOUTPUT plots the results in the specified filename.
    % Reads out and plots voltage, current and grid configurations at the
    % timestamps given.
    
    switch nargin
        case 0
            error('Too few arguments supplied.')
        case 1
            fname = varargin{1};
            minStep = 0;
        case 2
            fname = varargin{1};
            minStep = varargin{2};
        otherwise
            error('Too many arguments supplied.');
    end
    
    d = load(fname);
    grids = d.gridvec; gs = d.gs;
    t = d.tvec; V = d.Vvec; I = d.Ivec; e = d.evec;
    
    N = length(grids);
    
    % Get full range of T
    minT = Inf; maxT = 0;
    for n = 1:N
        thisMinT = min(grids(n).T);
        thisMaxT = max(grids(n).T);
        if thisMinT < minT; minT = thisMinT; end
        if thisMaxT > maxT; maxT = thisMaxT; end
    end
    
    inStep = 0;
    
    tLast = 0;
    
    for n = 1:N
        inStep = inStep + t(n) - tLast;
        tLast = t(n);
        if inStep < minStep && ~(n == N)
            continue;
        else
            inStep = 0;
        end
        
        typex = cell(numel(grids(n).x),6);
        typey = cell(numel(grids(n).y),6);
        typez = cell(numel(grids(n).z),6);
        
        for nn = 1:6
            typex{nn} = grids(n).x(grids(n).type == nn);
            typey{nn} = grids(n).y(grids(n).type == nn);
            typez{nn} = grids(n).z(grids(n).type == nn);
        end
        
        Qvb = (grids(n).Q(grids(n).type == 6)/1.602e-19)/2;
        
        clf;
        
        subplot(232)
            semilogy(t(1:n),I(1:n),'k.-', ...
                     t(e == 1),I(e == 1),'g+', ...
                     t(e == 2),I(e == 2),'b^', ...
                     t(e == 3),I(e == 3),'rx');
            title('Current vs. time');
            xlabel('t [ns]');
            ylabel('I [A]');
        subplot(233)
            plot(t,V,'w',t(1:n),V(1:n),'k.-');
            title('Voltage vs. time');
            xlabel('t [ns]');
            ylabel('V [V]');
        subplot(235)
            semilogy(V(1:n),I(1:n),'k.-', ...
                     V(e == 1),I(e == 1),'g+', ...
                     V(e == 2),I(e == 2),'b^', ...
                     V(e == 3),I(e == 3),'rx');
            title('Current vs. voltage');
            xlabel('V [V]');
            ylabel('I [A]');
        subplot(236)
            colormap hot;
            plotBlockData2([grids(n).x],[grids(n).y],[grids(n).z],[grids(n).T],gs,[minT maxT]);
            title('Temperature');
        
        subplot(2,3,[1 4])
            % lower
            plot3(typex{1},typey{1},typez{1},'k^', ...
                'MarkerSize',10,'MarkerFaceColor',[0.4 0.4 0.4])
            hold on;
            % upper
            plot3(typex{2},typey{2},typez{2},'kv', ...
                'MarkerSize',10,'MarkerFaceColor','none');
            % OEL
            plot3(typex{3},typey{3},typez{3},'ko', ...
                'MarkerSize',3,'MarkerFaceColor',[0.6 0.6 0.6]);
            % OEL_ox
            plot3(typex{4},typey{4},typez{4},'ko', ...
                'MarkerSize',10,'MarkerFaceColor',[1 1 1]);
            % O-2
            plot3(typex{5},typey{5},typez{5},'ko', ...
                'MarkerSize',10,'MarkerFaceColor','r');
            % V+2
            for nn = 1:length(Qvb)
            plot3(typex{6}(nn),typey{6}(nn),typez{6}(nn),'ko', ...
                'MarkerSize',10,'MarkerFaceColor',[1-Qvb(nn) 1-Qvb(nn) 1]);
            end
            hold off;
            
            % Build legend
%             typeNames = {'oxide','lower','upper','OEL','OEL_ox','O-2','V+2'};
%             legendLabels = {};
%             for nn = 2:length(typeNames)
%                 if any(gr(:) == nn-1) || any(gri(:,1) == nn-1)
%                     legendLabels = [legendLabels typeNames{nn}];
%                 end
%             end
            
            xlabel('x');
            ylabel('y');
            zlabel('z');
            title(['Grid at t = ' num2str(t(n),'%10.4es')]);
%             legend(legendLabels);
            view(-45,10);
            
        drawnow;
    end
    
end