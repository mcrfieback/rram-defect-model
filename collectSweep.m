function [t,I,V,e] = collectSweep(nameFormat,N,outName)
    % COLLECTSWEEP Collects the most important data from a large sweep.
    % Accepts a nameFormat with %d for run number, collects N runs and
    % saves them in outName.
    
    t = cell(N,1);
    I = cell(N,1);
    V = cell(N,1);
    
    for n = 1:N
        padZeros = repelem('0',floor(log10(N)) - floor(log10(n)));
        fname = sprintf(nameFormat,padZeros,n);
        if ~exist(fname,'file'); continue; end
        d = load(fname,'tvec','Ivec','Vvec');
        t{n} = d.tvec;
        I{n} = d.Ivec;
        V{n} = d.Vvec;
    end
    
    save(outName,'t','I','V');
end
