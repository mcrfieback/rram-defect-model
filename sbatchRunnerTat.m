function sbatchRunnerTat(T_amb,sweepLength,simLength)
    % Text format of single run shell script
    shellFormat = {
        '#!/bin/bash'
        ''
        '#SBATCH -J %s'
        '#SBATCH -o %s.out'
        '#SBATCH -e %s.err'
        '#SBATCH -p general'
        '#SBATCH -n 1'
        '#SBATCH -c 2'
        '#SBATCH -t %s'
        '#SBATCH --mem %s'
        '#SBATCH --mail-user=t.hol@student.tudelft.nl'
        '#SBATCH --mail-type=FAIL'
        ''
        'cd /home/tijshol/rram-defect-model/'
        'matlab -singleCompThread -batch "%s"'
    };

    % Turn cell array into char array with newlines
    shellFormat = [shellFormat mat2cell(repmat(newline,[numel(shellFormat) 1]),ones(numel(shellFormat),1))]';
    shellFormat = [shellFormat{:}];

    sweepName       = ['test-tat' num2str(T_amb)];
    outLocation     = '/home/tijshol/rram-defect-model/model_out/';
    scriptLocation  = '/home/tijshol/rram-defect-model/scripts/';
    
    %% Construct shell scripts

    outLocation = [outLocation sweepName '/'];
    scriptLocation = [scriptLocation sweepName '/'];

    mkdir(outLocation); mkdir(scriptLocation);

    scriptNames = cell(sweepLength,1);

    for n = 1:sweepLength
%         fprintf('Writing %d/%d...\n',n,N);
        padZeros = repelem('0',floor(log10(sweepLength)) - floor(log10(n)));
        runName = sprintf('%s-%s%d',sweepName,padZeros,n);

        % List the parameters for the peripheral script
        shellParams = {
            runName     % Job name
            runName     % Output file
            runName     % Error file
            '12:00:00'; % Runtime
            '1G';       % Memory
        };

        matlabCommand = sprintf('doTATTest(''%s.mat'',%d,%d,%d);', ...
            [outLocation runName],n,T_amb,simLength);

        % Build the script file
        scriptNames{n} = [runName '.sh'];
        scriptFile = [scriptLocation scriptNames{n}];
        fid = fopen(scriptFile,'w');
        if fid == -1; error('Error opening file: %s.',scriptFile); end
        fprintf(fid,shellFormat,shellParams{:},matlabCommand);
        fclose(fid);

        fileattrib(scriptFile,'+x','a');
    end

    %% Run shell scripts

    cd(scriptLocation);
    for n = 1:sweepLength
%         fprintf('Submitting %s\n',scriptNames{n});
        system(['sbatch ./' scriptNames{n}]);
    end
end
