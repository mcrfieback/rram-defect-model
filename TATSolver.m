function [I,P,Q,paths] = TATSolver(grid)
    % TATSOLVER Calculates the current through a grid of lattice_elements
    % assuming Trap Assisted Tunneling as the main conduction method.
    % Direct Tunneling is calculated in another function, DTSolver.

    global PROPS;
    props = struct();
    for fn = {'m_e','hbarOmega0_ox','hbarOmega0_il','phi_g0_ox','phi_g0_il','E_F_ox','E_F_il','S'}
        props.(fn{1}) = PROPS.(fn{1});
    end
    props.M = 10; % Limit of phonon sum
    
    showDebug = checkIfDebugging('TATSolver');
    
    q = 1.602e-19;
    P = [grid.P];
    Q = [grid.Q];
    
    persistent previousPaths; % The percolation paths of the previous run
    persistent previousI;     % The total current of the previous run
    persistent previousV;     % The applied voltage of the previous run
    
    if isempty(previousI); previousI = 0; end
    if isempty(previousV); previousV = 0; end
    
    % Build a list out of the vacancies in the grid
    traps_unsort = grid(grid.is('V+2'))';
    N_traps = length(traps_unsort);
    
    % Order the traps from lowest to highest potential (movement of electrons)
    [~,ind] = sort([traps_unsort.phi_ex]);
    traps = traps_unsort(ind);
    
    % Initiate electron occupation probability for traps
    f = zeros(size(traps));
    
    % Initiate paths storage cell
    paths = cell(0,2);
    
    % Get the upper and lower electrode potentials
    V_U = grid(find(grid.is('upper') & ~[grid.isIL], 1)).phi_ex;
    V_L = grid(find(grid.is('lower') & ~[grid.isIL], 1)).phi_ex;
    
    % Check which electrode is the anode
    isLowerAnode = V_U < V_L;
    
    if V_U == V_L
        I = 0;
        P = zeros(size(P));
        return;
    end
    
    if showDebug; fprintf('Calculating %d x %d rates...\n',N_traps,N_traps); end
    
    % Calculate all rates from the cathode and to the anode
    R_ca = zeros(N_traps,N_traps); % Rates from cathode via trap ii to trap jj
    f_ca = zeros(N_traps,N_traps); %  Electron occupation probability for trap ii
    R_an = zeros(N_traps,N_traps); % Rates from trap ii via trap jj to anode
    f_an = zeros(N_traps,N_traps); %  Electron occupation probability for trap jj
    R_ca_an = zeros(N_traps,1);    % Rates from cathode via trap ii to anode
    f_ca_an = zeros(N_traps,1);    %  Electron occupation probability for trap ii
    for ii = 1:N_traps
        if isLowerAnode
            cathode = grid(traps(ii).index_U);
            anode = grid(traps(ii).index_L);
        else
            cathode = grid(traps(ii).index_L);
            anode = grid(traps(ii).index_U);
        end
        [R_ca_an(ii), f_ca_an(ii)] = getRate(cathode,traps(ii),anode,props);
        for jj = (ii+1):N_traps
            [R_ca(ii,jj),f_ca(ii,jj)] = getRate(cathode,traps(ii),traps(jj),props);
            [R_an(ii,jj),f_an(ii,jj)] = getRate(traps(ii),traps(jj),anode,props);
        end
    end
    
    % Boolean vector that flags traps as being in a path
    trapInPath = false(length(traps),1);
    
    N_paths = 0;
    I = 0;

    %% Path finding loop
    
    while any(~trapInPath)
        N_paths = N_paths + 1;
        if showDebug
            progress = 100*nnz(trapInPath)/length(trapInPath);
            disp(['Calculating path ' num2str(N_paths) '... [' num2str(progress) '%]'])
        end
        
        % First traps considered are highest rate cathode --> trap --> trap
        R_ca_max = max(R_ca(~trapInPath,~trapInPath),[],'all');
        R_ca_an_max = max(R_ca_an(~trapInPath));
        
        if (R_ca_max == 0) && (R_ca_an_max == 0)
            if showDebug; disp('No more traps can be reached from the cathode, exiting...'); end
            break;
        end
        
        if R_ca_an_max > R_ca_max
            % Single trap path
            
            currentTrap = find(R_ca_an == R_ca_an_max,1);
            trapInPath(currentTrap) = true;
            
            R_min = R_ca_an_max;
            
            f(currentTrap) = f_ca_an(currentTrap);
            
            path = currentTrap;
            if showDebug; fprintf('[%d > %d > %d]: single trap path\n',0,currentTrap,N_traps+1); end
        else
            % Multiple traps path
            [lastTrap, currentTrap] = find(R_ca == R_ca_max,1);
            trapInPath(lastTrap) = true;
            
            R_min = R_ca_max;
            
            f(lastTrap) = f_ca(lastTrap,currentTrap);

            path = [lastTrap currentTrap];
            nextTrap = -1;
            
            if showDebug; fprintf('[%d > %d > %d]: multiple trap path\n',0,lastTrap,currentTrap); end

            %% Trap finding loop
            while nextTrap ~= N_traps+1 % While path has not arrived at anode
                trapInPath(currentTrap) = true;

                % Calculate rates from currentTrap to all other traps not in the path
                R_ctrap = zeros(N_traps,1);
                f_ctrap = zeros(N_traps,1);
                for ii = (currentTrap+1):N_traps
                    if ~trapInPath(ii)
                        [R_ctrap(ii),f_ctrap(ii)] = getRate(traps(lastTrap),traps(currentTrap),traps(ii),props);
                    end
                end

                % Find the next trap in the path
                % (Similarly in 3 lines due to same issue as before)
                [R_next, nextTrap] = max(R_ctrap);

                % Check if either:
                % - All traps are in a path
                % - Rate to anode is larger than to unmarked traps
                % - There is no rate to unmarked traps > 0
                % If so, set exit condition
                if isempty(R_next) || (R_an(lastTrap,currentTrap) > R_next) || (R_next == 0)
                    R_next = R_an(lastTrap,currentTrap);
                    nextTrap = N_traps+1;
                else
                    path = [path nextTrap];
                end

                if (nextTrap ~= N_traps+1) && (trapInPath(nextTrap))
                    error('Selected a trap that is already in the path.');
                end
                
                if showDebug; fprintf('[%d > %d > %d]: multiple trap path\n',lastTrap,currentTrap,nextTrap); end

%                 if nextTrap ~= N_traps+1
%                 plot3([traps.x],[traps.y],[traps.z],'k.', ...
%                       [traps([lastTrap currentTrap nextTrap]).x], ...
%                       [traps([lastTrap currentTrap nextTrap]).y], ...
%                       [traps([lastTrap currentTrap nextTrap]).z], 'm*-');
%                   pause;
%                 end
                
                % Set the new minimum rate if it is smaller
                if R_next < R_min
                    R_min = R_next; 
                end
                
                % Move to the next trap
                lastTrap = currentTrap;
                currentTrap = nextTrap;
            end
        end
        
        I = I + q*R_min;
        
        if showDebug; disp(['I_path = ' num2str(q*R_min) '; path: ' num2str(path)]); end
        paths{N_paths,1} = path;
        paths{N_paths,2} = R_min;
    end
    
    %% Local maximum checker and fixer
    
    if (abs(V_U - V_L) > previousV && I < previousI) ...
    || (abs(V_U - V_L) < previousV && I > previousI)
        % The current is going the wrong way: a local maximum was selected in a percolation path
        % Recalculate everything using the previous paths
        warning('Unexpected current decrease/increase (%.4e -> %.4e): suspected local maximum, recalculating using previous paths.',previousI,I);
        
        pathsOld = paths;
        IOld = I;
        
        I = 0;
        P = [grid.P];
        f = zeros(size(traps));
        
        paths = cell(0,2);
        
        for n = 1:length(previousPaths)
            % Find trap indices in traps list
            path = previousPaths{n};
            for nn = 1:length(path)
                path(nn) = find([traps.index] == path(nn));
            end
            
            if length(path) == 1
                % Single trap path
                R_min = R_ca_an(path);
                f(path) = f_ca_an(path);
            elseif length(path) == 2
                % Double trap path
                R_min = min(R_ca(path(1),path(2)),R_an(path(1),path(2)));
                f(path) = [f_ca(path(1),path(2)) f_an(path(1),path(2))];
            elseif length(path) > 2
                % Multiple trap path
                R_min = R_ca(path(1),path(2));
                f(path(1)) = f_ca(path(1),path(2));
                for nn = 1:length(path)-2
                    [R_ctrap,f_ctrap] = getRate(traps(path(nn)),traps(path(nn+1)),traps(path(nn+2)),props);
                    f(path(nn+1)) = f_ctrap;
                    R_min = min(R_min,R_ctrap);
                end
                R_min = min(R_min,R_an(path(1),path(2)));
                f(path(end)) = f_an(path(end-1),path(end));
            else
                error('Path length incorrect: %d',length(path));
            end
            
            I = I + q*R_min;
            
            paths{n,1} = path;
            paths{n,2} = R_min;
        end
    end
    
    if (abs(V_U - V_L) > previousV && I < previousI) ...
    || (abs(V_U - V_L) < previousV && I > previousI)
        warning('Current is still unexpectedly decreasing/increasing (%.4e -> %.4e).',previousI,I');
        if (abs(V_U - V_L) > previousV && I < IOld) ...
        || (abs(V_U - V_L) < previousV && I > IOld)
            warning('First option worked better.');
            I = IOld;
            paths = pathsOld;
        end
    end
    
    previousPaths = cell(size(paths(:,1)));
    for n = 1:length(paths(:,1))
        previousPaths{n} = [traps(paths{n,1}(1:end)).index];
    end
    previousI = I;
    previousV = abs(V_U - V_L);
        
    %% Set power and charge
    
    % Set dissipated power
    for n = 1:length(paths(:,1))
        [path, R_min] = paths{n,:};
        firstTrap = traps(path(1));   % The first trap of the path
        lastTrap = traps(path(end));  % The last trap of the path
        if isLowerAnode
            % The start is the upper electrode
            pathStart = grid(firstTrap.index_U);
            pathEnd = grid(lastTrap.index_L);
        else
            % The start is the lower electrode
            pathStart = grid(firstTrap.index_L);
            pathEnd = grid(lastTrap.index_U);
        end
        % Set the power dissipated in the first trap
        P(firstTrap.index) = R_min*(q+(-q*pathStart.phi_ex-pathStart.E) - (-q*firstTrap.phi_ex-firstTrap.E));
        % Set the power dissipated in the electrode (IL) at the end
        P(pathEnd.index) = R_min*(q+(-q*lastTrap.phi_ex-lastTrap.E) - (-q*pathEnd.phi_ex-pathEnd.E));
        % Set the power dissipated in the rest of the traps in the path
        for step = 2:length(path)
            P(traps(path(step)).index) = R_min*(q+(-q*traps(path(step-1)).phi_ex-traps(path(step-1)).E) ...
                - (-q*traps(path(step)).phi_ex-traps(path(step)).E));
        end

        P(P<0) = 0;
    end
    
    % Set charge
    Q([traps.index]) = q*(1-f);
    
    if showDebug
        disp(['Total current is ' num2str(I) 'A through ' num2str(N_paths) ' percolation paths.']);
    end
    
    if any(P < 0,'all')
        warning('Detected negative power.');
    end
    
     %% Percolation paths plotter
%     if checkIfDebugging('TATSolver plot')
%         pmin = min([grid.x]);
%         xmax = max([grid.x]);
%         ymax = max([grid.y]);
%         zmax = max([grid.z]);
%         
%         oldpathx = NaN(N_traps+2,N_paths);
%         oldpathy = NaN(N_traps+2,N_paths);
%         oldpathz = NaN(N_traps+2,N_paths);
%         
%         trapLabels = cell(N_traps,1);
%         
%         stackArea = (pmin)^2*(size(grid,1)*size(grid,2));
%         
%         for n = 1:N_traps; trapLabels{n} = num2str(n); end
% 
%         for n = 1:N_paths
%             path = paths{n,1};
%             botnLoc = paths{n,3};
%             if isLowerAnode
%                 pathx = [grid(traps(path(2)).index_U).x traps(path(2:end-1)).x grid(traps(path(end-1)).index_L).x];
%                 pathy = [grid(traps(path(2)).index_U).y traps(path(2:end-1)).y grid(traps(path(end-1)).index_L).y];
%                 pathz = [grid(traps(path(2)).index_U).z traps(path(2:end-1)).z grid(traps(path(end-1)).index_L).z];
%                 icon = 'v';
%             else
%                 pathx = [grid(traps(path(2)).index_L).x traps(path(2:end-1)).x grid(traps(path(end-1)).index_U).x];
%                 pathy = [grid(traps(path(2)).index_L).y traps(path(2:end-1)).y grid(traps(path(end-1)).index_U).y];
%                 pathz = [grid(traps(path(2)).index_L).z traps(path(2:end-1)).z grid(traps(path(end-1)).index_U).z];
%                 icon = '^';
%             end
% 
%             botnx = pathx([botnLoc botnLoc+1]);
%             botny = pathy([botnLoc botnLoc+1]);
%             botnz = pathz([botnLoc botnLoc+1]);
% 
%             plot3([traps.x],[traps.y],[traps.z],'bx', ...
%                   oldpathx, oldpathy, oldpathz, 'k:*', ...
%                   pathx,pathy,pathz,[icon 'm--'], ...
%                   botnx,botny,botnz,'r' ...
%                   );
%             
%             title(['I_{perc} = ' num2str(paths{n,2}) 'A'])
%             patch([pmin xmax xmax pmin],[pmin pmin ymax ymax],[pmin pmin pmin pmin])
%             line([pmin xmax xmax pmin pmin],[pmin pmin ymax ymax pmin],[zmax zmax zmax zmax zmax]);
%             text([traps.x],[traps.y],[traps.z]+pmin/2,trapLabels);
%             view(340,10);
%             pause(0.2);
%             
%             oldpathx(1:length(pathx),n) = pathx;
%             oldpathy(1:length(pathy),n) = pathy;
%             oldpathz(1:length(pathz),n) = pathz;
%         end
%         
%         plot3([traps.x],[traps.y],[traps.z],'bx', ...
%               oldpathx, oldpathy, oldpathz, 'k:*' ...
%               );
%         title(['I_{tot} = ' num2str(I) 'A; J_{tot} = ' num2str(I/stackArea * 1e-4,'%10.4e') 'A/cm^2']);
%         patch([pmin xmax xmax pmin],[pmin pmin ymax ymax],[pmin pmin pmin pmin])
%         line([pmin xmax xmax pmin pmin],[pmin pmin ymax ymax pmin],[zmax zmax zmax zmax zmax]);
%             text([traps.x],[traps.y],[traps.z]+pmin/2,trapLabels);
%         view(340,10);
%     end
end

function [R,f] = getRate(from,trap,to,props)
    % Get the rate of electrons passing from trap "from", via trap "trap",
    % to trap "to"
    
    q = 1.602e-19;
    hbarOmega0 = props.hbarOmega0_ox * q;
    m_e = props.m_e*9.110e-31;  % Effective electron mass
    
    M = props.M; % Limit of phonon sum

    tau_c = 0; tau_e = 0;
    
    for n = 0:M
        % Capture and emission rates
        [Ca_j_n, Em_j_n] = calc_CaEm(trap,n,props);
        % Density of states
        N_n = calc_N(from,trap,n,props);
        if imag(N_n) > 0; warning(['Complex N detected from ' num2str(from.index) ' to ' num2str(trap.index) '(' num2str(n) ' phonons)']); end
        N_p = calc_N(to,trap,n,props);
        if imag(N_p) > 0; warning(['Complex N detected from ' num2str(to.index) ' to ' num2str(trap.index) '(' num2str(n) ' phonons)']); end
        % Phonon occupation probability
        f_n = calc_f(from,trap,n,props);
        f_p = calc_f(to,trap,n,props);
        % Tunneling probability
        P_T_n = getTunnelingProbability(from,trap,n*hbarOmega0,m_e);
        if imag(P_T_n) > 0; warning(['Complex P_T detected from ' num2str(from.index) ' to ' num2str(to.index) '(' num2str(n) ' phonons)']); end
        P_T_p = getTunnelingProbability(trap,to,n*hbarOmega0,m_e);
        if imag(P_T_p) > 0; warning(['Complex P_T detected from ' num2str(from.index) ' to ' num2str(to.index) '(' num2str(n) ' phonons)']); end
        % (inverted) Capture and emission times for n phonons
        tau_c_j_n = N_n * (f_n) * Ca_j_n * P_T_n;
        tau_e_j_n = N_p * (1-f_p) * Em_j_n * P_T_p;

        % Add to total (inverted) capture and emission times
        tau_c = tau_c + tau_c_j_n;
        tau_e = tau_e + tau_e_j_n;
    end
    
    % Set the charge rate
    R = 1/(1/tau_c + 1/tau_e);
    f = R/tau_e;
    
%     fprintf('Ca:  %.4e\nEm:  %.4e\nN:   %.4e\nf:   %.4e\nP_T: %.4e\n',Ca_j_n,Em_j_n,N_p,f_p,P_T_p);
%     fprintf('[%s > %s > %s] - tau_c: %.2e, tau_e: %.2e, f: %.5f\n',from.type,trap.type,to.type,tau_c,tau_e,100*f);
end

function N = calc_N(at,in,n,props)
    % Calculate the density of states at "at" for an electron incoming from
    % "in"
    
    q = 1.602e-19;              % Elemental charge
    hbar = 1.055e-34;           % Modified Planck constant
    m_e = props.m_e*9.110e-31;  % Electron effective mass
    
    if in.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
    end
    
    energyDifference = (-in.phi_ex*q - in.E) + n*hbarOmega0 - (-at.phi_ex*q - at.E);
    
    if energyDifference < 0
        N = 0;
    else
        N = 1/(2*pi^2)*(2*m_e)^(3/2)/hbar^3*sqrt(energyDifference);
    end
end

function f = calc_f(at,in,n,props)
    % Calculate the Fermi-Dirac occupation probability at "at" for an
    % electron incoming from "in"
    
    q = 1.602e-19;          % Elemental charge
    k = 1.381e-23;          % Boltzmann constant
    
    if in.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
    end
    
    if at.isIL
        E_F = props.E_F_il * props.phi_g0_il * q;
    else
        E_F = props.E_F_ox * props.phi_g0_ox * q;
    end
    
%     fprintf('%.3f - %.3f\n',(-from.phi_ex*q - from.E + q)/q,(-to.phi_ex*q - E_F)/q)
    
    f = 1/(1 + exp(((-in.phi_ex*q - in.E) + n*hbarOmega0 - (-at.phi_ex*q - E_F))/(k*at.T)));
end

function [Ca, Em] = calc_CaEm(trap,n,props)
    % Calculate capture and emission rates of trap "trap" given n phonons
    
    q = 1.602e-19;              % Elemental charge
    hbar = 1.055e-34;           % Modified Planck constant
    m_e = props.m_e*9.110e-31;  % Electron effective mass
    k = 1.381e-23;              % Boltzmann constant
    S = props.S;                % Huang-Rhys factor
    
    if trap.isIL
        hbarOmega0 = props.hbarOmega0_il * q;
        phi_g0 = props.phi_g0_il * q;
    else
        hbarOmega0 = props.hbarOmega0_ox * q;
        phi_g0 = props.phi_g0_ox * q;
    end
    
    % For electrodes, trap radius is actually 0, but this will only reflect 
    % in the tunneling probability. Since in Vandelli/Larcher c_0 is 
    % assumed a constant, R_T for electrodes is set the same value as traps
    if trap.isIL; R_T = sqrt(1e-18/pi); else; R_T = trap.R_T; end
    
    % Constant dependent on field
    c_0 = ((4*pi)^2)*(R_T^3)*(q^2)*(trap.F_ex^2)*hbar ... 
        / (phi_g0 * 2*m_e);
    
    % Bose function
    f_B = 1/(exp(hbarOmega0/(k*trap.T)) - 1);
    
    % Bessel function
    I_n = besseli(n, 2*S*sqrt(f_B*(f_B+1)));
    
    % Multiphonon transition probability
    L = ((f_B + 1)/f_B)^(n/2)*exp(-S*(2*f_B+1))*I_n;
    
    % Capture rate
    Ca = c_0*L;
    
    % Emission rate
    Em = Ca*exp(-n*hbarOmega0/(k*trap.T));
end
