function plotBlockData(varargin)
    if nargin < 4 && (nargin ~= 1)
        error('Wrong amount of arguments supplied.')
    elseif nargin == 6
        labels = varargin{5};
        mode = varargin{6};
    elseif nargin == 5
        if isa(varargin{5},'cell')
            labels = varargin{5};
            mode = 'default';
        else
            labels = {};
            mode = varargin{5};
        end
    else
        labels = {};
        mode = 'default';
    end
        
    if nargin == 1
        X = varargin{1};
        [M,N,P] = size(X);
        x = 1:M; y = 1:N; z = 1:P;
    else
        [x,y,z,X] = varargin{1:4};
        [M,N,P] = size(X);
    end
    
    
    N_plots = [1, M, N, P];
    N_plot = N_plots(strcmp(mode,{'default','sweepx','sweepy','sweepz'}));
    
    for n = 1:N_plot
        if strcmp(mode,'sweepx')
            h = slice(X,n,[],1);
            set(h,'EdgeColor','none');
        elseif strcmp(mode,'sweepy')
            h = slice(X,[],n,1);
            set(h,'EdgeColor','none');
        elseif strcmp(mode,'sweepz')
            h = slice(X,1,1,n);
            set(h,'EdgeColor','none');
        else
            hold on;
            h = slice(X(1:floor(M/2),1:N,1:P), ...
                M,[1 floor(N/2)],[1 P], ...
                'nearest');
            set(h,'EdgeColor','none');
            h = slice(X(1:M,1:floor(N/2),1:P), ...
                [1 floor(M/2)],N,[1 P], ...
                'nearest');
            set(h,'EdgeColor','none');
            hold off;
        end
        clb = colorbar;

        view(137,23);
        axis([0 M+1 0 N+1 0 P+1]);
        % Changing the axis labels to the proper values like this is a dirty hack.
        % I fully blame the incapability of the slice function for this.
        if nargin ~=1
            xlabel('x');
            xticks([1 floor(M/2) M])
            xticklabels({[num2str(x(1)*1e9) ' nm'], [num2str(x(floor(M/2))*1e9) ' nm'], [num2str(x(M)*1e9) ' nm']});
            ylabel('y');
            yticks([1 floor(N/2) N])
            yticklabels({[num2str(y(1)*1e9) ' nm'], [num2str(y(floor(N/2))*1e9) ' nm'], [num2str(y(N)*1e9) ' nm']});
            zlabel('z');
            zticks([1 floor(P/2) P])
            zticklabels({[num2str(z(1)*1e9) ' nm'], [num2str(z(floor(P/2))*1e9) ' nm'], [num2str(z(P)*1e9) ' nm']});
        end
        
        if ~isempty(labels)
            clb.Ticks = 1:length(labels);
            clb.TickLabels = labels;
        end
    end
end
