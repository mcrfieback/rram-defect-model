function plotTunnelingProbability(trap_i, trap_j, E)
    % PLOTTUNNELINGPROBABILITY Plots the potential barrier and particle
    % energy level E in the case of tunneling from trap i to trap j.
    
    q = 1.602e-19;          % Elemental charge
    global PROPS;
    
    m_e = PROPS.m_e*9.110e-31;  % Electron effective mass
    
    % Potential barrier bounds
    U_i = trap_i.E;
    U_j = U_i - (trap_j.phi_ex - trap_i.phi_ex)*q;
    
    % Get (precalc) shortest distance between trap and electrode (if elec.)
    if trap_i.is('upper')
        % Trap i is the upper electrode
        D_ij = trap_j.d_U;
    elseif trap_j.is('upper')
        % Trap j is the upper electrode
        D_ij = trap_i.d_U;
    elseif trap_i.is('lower')
        % Trap i is the lower electrode
        D_ij = trap_j.d_L;
    elseif trap_j.is('lower')
        % Trap j is the lower electrode
        D_ij = trap_i.d_L;
    else
        % Calculate distance between trap i and trap j
        D_ij = sqrt((trap_j.x - trap_i.x)^2 ...
                + (trap_j.y - trap_i.y)^2 ...
                + (trap_j.z - trap_i.z)^2);
    end
    
    % Trap j energy
    E_Tj = trap_j.E;
    
    % Force gradient
    F_ij = (U_i - U_j)/D_ij;
    
    % Capture radii of traps and potential value at radii
    R_i = trap_i.R_T;
    R_j = trap_j.R_T;
    
    N = 1000;
    x = linspace(0,D_ij,N)';
    U = zeros(N,1);
    
    if D_ij < (R_i + R_j)
        if R_i == 0
            U = U_j - E_Tj + (E_Tj/R_j + F_ij) * (D_ij - x);
        elseif R_j == 0
            U = (U_i/R_i - F_ij) * x;
        else
            xcross = (U_j - E_Tj + (E_Tj/R_j + F_ij)*D_ij) ... 
                   / (U_i/R_i + E_Tj/R_j);

            xrange1 = x <= xcross;
            xrange2 = x > xcross; 

            U(xrange1) = (U_i/R_i - F_ij) * x(xrange1);
            U(xrange2) = U_j - E_Tj + (E_Tj/R_j + F_ij) * (D_ij - x(xrange2));
        end
    else
        xrange1 = x < R_i;
        xrange2 = ~~((x >= R_i) .* (x <= (D_ij - R_j)));
        xrange3 = x > (D_ij - R_j) ;
        
        U(xrange1) = (U_i/R_i - F_ij) * x(xrange1);
        U(xrange2) = U_i - F_ij*x(xrange2);
        U(xrange3) = U_j - E_Tj + (E_Tj/R_j + F_ij) * (D_ij - x(xrange3));
    end
    
    x_ext = 1e9*[linspace(-D_ij,0,N)'-D_ij/(N-1); x; linspace(D_ij,2*D_ij,N)'+D_ij/(N-1)];

    if trap_i.is({'upper','lower'})
        Ui_ext = zeros(N,1);
    else
        Ui_ext = [ones(N-floor(N/5),1)*U_i; zeros(floor(N/5),1)];
    end
    
    if trap_j.is({'upper','lower'})
        Uj_ext = (U_j-E_Tj)*ones(N,1);
    else
        Uj_ext = [ones(floor(N/5),1)*(U_j-E_Tj); ones(N-floor(N/5),1)*U_j];
    end
    
    P_T = getTunnelingProbability(trap_i,trap_j,E,m_e);
    
    plot(x_ext,[Ui_ext; U; Uj_ext]/q,'-', ...
        [x_ext(1) x_ext(end)],[E E]/q,':', ...
        x_ext,[U_i*ones(N,1); (U_i - F_ij*x); U_j*ones(N,1)]/q, '--');
    xlabel('x [nm]')
    ylabel('Energy [eV]');
    title(['Potential barrier: P_T = ' num2str(P_T)]);
    
    legend( ...
        'U', ...
        'E', ...
        '\phi', ...
        'Location', 'West' ...
    );
end

