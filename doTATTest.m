function doTATTest(fname,seed,T_amb,N)

    rng(seed);

    lat_props = struct( ...
        'd_x', 20e-9, ...
        'd_y', 20e-9, ...
        'd_z', 5e-9, ...
        'grid_type', 'regular', ...
        'vacancyDistribution', 4.5e19 ...
    );

    setProps('E_C_il',3.1,'E_Tmu',1.9,'E_Tdelta',0.5,'S',17);
    
    gridr = buildReducedLatticeGrid(lat_props,0,0,T_amb);

    oxideThickness = lat_props.d_z;

    V = linspace(0.05,2,N);

    I = zeros(N,1);

    for n = 1:N
        fprintf('%d/%d\n',n,N);
        F = V(n)/oxideThickness;
        for ii = 1:(size(gridr,1)-1)
            gridr(ii,1).F_ex = F;
            gridr(ii,1).phi_ex = V(n);

            gridr(ii,2).phi_ex = gridr(ii,2).z*F;
            gridr(ii,2).F_ex = F;

            gridr(ii,3).F_ex = F;
        end
        gridr(end,1).phi_ex = V(n);
        gridr(end,2).phi_ex = V(n);
        gridr(end,3).phi_ex = 0;

        I(n,1) = TATSolver(gridr);

        if isnan(I(n,1))
            error('I is NaN');
        end
    end

    J = I/(lat_props.d_x*lat_props.d_y*1e4);
    
    save(fname,'J');
end

