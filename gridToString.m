function str = gridToString(grid,gs)
    %GRIDTOSTRING Translate lattice grid types to a string for saving in a
    %text file
    
    % Convert types to numbers
    ids = zeros(1,numel(grid));
    types = {'oxide', 'lower', 'upper', 'OEL', 'OEL_ox', 'O-2', 'V+2'};
    for n = 2:length(types) % Leave out oxide (id = 0)
        ids = ids + (n-1)*strcmp({grid.type},types(n));
    end
    
    % Compress and write regular grid
    str = '';
    lastSymbol = ids(1);
    count = 0;
    
    for n = 1:(gs(1)*gs(2)*gs(3))
        if lastSymbol ~= ids(n)
            str = [str num2str(lastSymbol) ';' num2str(count) ' '];
            count = 1;
        else
            count = count + 1;
        end
        lastSymbol = ids(n);
    end
    str = [str num2str(lastSymbol) ';' num2str(count) ' ;; '];
    
    % Write interstitial grid
    for n = (n+1):numel(grid)
        str = [str num2str(ids(n)) num2str(grid(n).intAt(1)','[%d]') ' '];
    end
    
    str = [str ';;'];
end

