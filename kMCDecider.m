function [event, dt] = kMCDecider(grid)
    % KMCDECIDER Decides the next event to occur and returns the time step
    % using kinetic Monte Carlo on the provided lattice grid to calculate 
    % event rates and pick an event.
    
    global PROPS;
    % -- Constants (converted to SI units)
    q = 1.602e-19;                  % [C] Elemental charge
    k = 1.381e-23;                  % [m^2 kg s^-2 K^-1] Boltzmann constant
    
    E_A = PROPS.E_A*q;              % [J] O-Hf bond breakage zero-field effective activation energy
    E_AD_i = PROPS.E_AD_i*q;        % [J] Diffusion activation energy (ion)
    E_AR = PROPS.E_AR*q;            % [J] Recombination activation energy
    
    p_0 = PROPS.p_0*1e-10*q;        % [Cm]  O-Hf dipole moment
    b = p_0*((2+PROPS.epsilon)/3);  % [Cm] O-Hf bond polarization factor
    nu = PROPS.nu;                  % [Hz] Effective vibration frequency of O-Hf bonds
    k_D = PROPS.k_D*1e-10*q;        % [Cm] Jump distance/2 times charge of diffusing specie
    
    % TODO: make parameter
    gs = [30 30 40];
    
    % Deduct reduced grid with only oxides and O-2
    gridOxide = grid(grid.is('oxide'));
    gridIons = grid(grid.is('O-2'));
    
    % Oxide rates array (index + max 6 events per grid point)
    ratesOxide = zeros(numel(gridOxide),7);
    
    % Ion rates array (index + max 12 events per grid point)
    ratesIons = zeros(numel(gridIons),13);
    
    % Straight directions
    dirs = [
        -1 0 0
        0 -1 0
        0 0 -1
        1 0 0
        0 1 0
        0 0 1
        ];
    
    % Check regular grid (oxide and V+2)
    for n = 1:size(ratesOxide,1)
        gp = gridOxide(n);
        [ii,jj,kk] = ind2sub(gs,gp.index);
        generationIsPossibleTo = [
            ii ~= 1     && grid(sub2ind(gs,ii-1,jj,kk)).is('oxide')
            jj ~= 1     && grid(sub2ind(gs,ii,jj-1,kk)).is('oxide')
            kk ~= 1     && grid(sub2ind(gs,ii,jj,kk-1)).is('oxide')
            ii ~= gs(1) && grid(sub2ind(gs,ii+1,jj,kk)).is('oxide')
            jj ~= gs(2) && grid(sub2ind(gs,ii,jj+1,kk)).is('oxide')
            kk ~= gs(3) && grid(sub2ind(gs,ii,jj,kk+1)).is('oxide')
        ];
        
        % -- EVENT: Vacancy/ion Frenkel pair generation
        % Get projection of F_vec on generation directions
        F_proj = dirs*gp.F_vec_ex;
        % NB: Sign of b is reversed since O-2 is negatively charged
        G_F = nu * exp(-(E_A + b*F_proj)/(k*gp.T));

        % Prevent inverse temperature relations TODO: VERIFY
        G_F(G_F > nu) = nu;

        % Save rates in rates array
        ratesOxide(n,:) = [gp.index generationIsPossibleTo'.*G_F'];
    end
            
    for n = 1:size(ratesIons,1)
        gp = gridIons(n);
        [ii,jj,kk] = ind2sub(gs,gp.index);
        
        % Check if diffusion is possible to other interstitial positions
        % Diffusion is possible if there's no grid point or it's a V+2
        diffusionIsPossibleTo = [
            ii == 1     || grid(sub2ind(gs,ii-1,jj,kk)).is('oxide')
            jj == 1     || grid(sub2ind(gs,ii,jj-1,kk)).is('oxide')
            kk == 1     || grid(sub2ind(gs,ii,jj,kk-1)).is('oxide')
            ii == gs(1) || grid(sub2ind(gs,ii+1,jj,kk)).is('oxide')
            jj == gs(2) || grid(sub2ind(gs,ii,jj+1,kk)).is('oxide')
            kk == gs(3) || grid(sub2ind(gs,ii,jj,kk+1)).is('oxide')
        ];

        % Check if recombination is possible for all 8 directions
        % Recombination is possible if there's a grid point and it's a V+2
        recombinationIsPossibleTo = [
            ii ~= 1     && grid(sub2ind(gs,ii-1,jj,kk)).is('V+2')
            jj ~= 1     && grid(sub2ind(gs,ii,jj-1,kk)).is('V+2')
            kk ~= 1     && grid(sub2ind(gs,ii,jj,kk-1)).is('V+2')
            ii ~= gs(1) && grid(sub2ind(gs,ii+1,jj,kk)).is('V+2')
            jj ~= gs(2) && grid(sub2ind(gs,ii,jj+1,kk)).is('V+2')
            kk ~= gs(3) && grid(sub2ind(gs,ii,jj,kk+1)).is('V+2')
        ];

        % -- EVENT: Ion diffusion in the 6 positive and negative xyz directions
        % Get projection of F_vec on drift directions
        F_proj = dirs*gp.F_vec;
        % NB: Sign of k_D is reversed since O-2 is negatively charged
        R_D = nu * exp(-(E_AD_i + k_D*F_proj)/(k*gp.T));

        % Prevent inverse temperature relations TODO: VERIFY
        R_D(R_D > nu) = nu;

        ratesIons(n,1:7) = [gp.index diffusionIsPossibleTo'.*R_D'];

        % -- EVENT: Recombination to any neighboring vacancy
        if any(recombinationIsPossibleTo)
            R_R = nu * exp(-(E_AR)/(k*gp.T));

            ratesIons(n,8:13) = recombinationIsPossibleTo'*R_R;
        end
    end
    
    % The time step is determined by the total rate
    R_tot = sum(ratesOxide(:,2:end),'all') + sum(ratesIons(:,2:end),'all');
    dt = 1/R_tot;

    % Pick a random event rate
    U = rand;
    R_pick = U * R_tot;
    
    % Find the corresponding event
    R_acc = 0;
    breakFlag = false;
    
    % Check for the event in oxide rates
    for ii = 1:size(ratesOxide,1)
        for jj = 2:7
            if ratesOxide(ii,jj) == 0; continue; end
            R_acc = R_acc + ratesOxide(ii,jj);
            if R_acc > R_pick
                event = struct(...
                    'type','G_F', ...
                    'index',ratesOxide(ii,1), ...
                    'to',jj-1);
                breakFlag = true; break;
            end
        end
        if breakFlag; break; end
    end
    
    % Check for the event in ion rates
    if ~breakFlag
        for ii = 1:size(ratesIons,1)
            % Check for drift events (col. 2 to 7)
            for jj = 2:7
                if ratesIons(ii,jj) == 0; continue; end
                R_acc = R_acc + ratesIons(ii,jj);
                if R_acc > R_pick
                    event = struct(...
                        'type','R_Di', ...
                        'index',ratesIons(ii,1), ...
                        'to',jj-1);
                    breakFlag = true; break;
                end
            end
            if breakFlag; break; end
            % Check for recombination events (col. 8 to 13)
            for jj = 8:13
                if ratesIons(ii,jj) == 0; continue; end
                R_acc = R_acc + ratesIons(ii,jj);
                if R_acc > R_pick
                    event = struct(...
                        'type','R_R', ...
                        'index',ratesIons(ii,1), ...
                        'to',jj-7);
                    breakFlag = true; break;
                end
            end
            if breakFlag; break; end
        end
    end
    
    if ~exist('event','var')
        error('Event unassigned');
    end
end
