function sbatchRunner(sweepName,sweepVar,sweepVals,simLength)
    % Text format of single run shell script
    shellFormat = {
        '#!/bin/bash'
        ''
        '#SBATCH -J %s'
        '#SBATCH -o %s.out'
        '#SBATCH -e %s.err'
        '#SBATCH -p general'
        '#SBATCH -n 1'
        '#SBATCH -c 2'
        '#SBATCH -t %s'
        '#SBATCH --mem %s'
        '#SBATCH --mail-user=t.hol@student.tudelft.nl'
        '#SBATCH --mail-type=FAIL'
        ''
        'cd /home/tijshol/rram-defect-model/'
        'matlab -singleCompThread -batch "%s"'
    };

    % Turn cell array into char array with newlines
    shellFormat = [shellFormat mat2cell(repmat(newline,[numel(shellFormat) 1]),ones(numel(shellFormat),1))]';
    shellFormat = [shellFormat{:}];

    sweepLength = length(sweepVals);

    %% Construct shell scripts
    sweepOutLocation     = ['/home/tijshol/rram-defect-model/model_out/' sweepName '/'];
    sweepScriptLocation  = ['/home/tijshol/rram-defect-model/scripts/' sweepName '/'];

    for sweep = 1:sweepLength
        
        padZeros = repelem('0',floor(log10(sweepLength)) - floor(log10(sweep)));
        simName = sprintf('%s-%s%d',sweepName,padZeros,sweep);
        
        simOutLocation = [sweepOutLocation simName '/'];
        simScriptLocation = [sweepScriptLocation simName '/'];

        mkdir(simOutLocation); mkdir(simScriptLocation);

        scriptNames = cell(simLength,1);

        for sim = 1:simLength
            padZeros = repelem('0',floor(log10(simLength)) - floor(log10(sim)));
            runName = sprintf('%s-%s%d',simName,padZeros,sim);

            % List the parameters for the peripheral script
            shellParams = {
                runName     % Job name
                runName     % Output file
                runName     % Error file
                '12:00:00'; % Runtime
                '1G';       % Memory
            };

            % List the parameters for the model run
            % (enter as numeric or char array)
            modelParams = struct(...
                'doPlot', 0, ...
                'doExit', 1, ...
                'saveDevice', +(sim == 1), ...
                'grid_type', 'filament', ...
                'rngSeed', simLength*(sweep-1)+sim, ...
                sweepVar, sweepVals{sweep} ...
            );

            % Build the MATLAB command
            matlabCommand = ['modelHandler(''' simOutLocation runName '.mat'''];
            for f = fields(modelParams)'
                v = modelParams.(f{1});
                if isnumeric(v)
                    argf = '%s,''%s'',%d';
                elseif ischar(v)
                    argf = '%s,''%s'',''%s''';
                else
                    error('Unsupported type in model parameters.');
                end
                matlabCommand = sprintf(argf,matlabCommand,f{1},v);
            end
            matlabCommand = [matlabCommand ');'];

            % Build the script file
            scriptNames{sim} = [runName '.sh'];
            scriptFile = [simScriptLocation scriptNames{sim}];
            fid = fopen(scriptFile,'w');
            if fid == -1; error('Error opening file: %s.',scriptFile); end
            fprintf(fid,shellFormat,shellParams{:},matlabCommand);
            fclose(fid);

            fileattrib(scriptFile,'+x','a');
        end

        %% Run shell scripts

        cd(simScriptLocation);
        for sim = 1:simLength
            [~, out] = system('squeue | wc -l');
            jobs = str2double(out);
            while jobs > 8000
                fprintf('Queue full: %d jobs. Waiting 10s...',jobs);
                pause(10)
            end
            system(['sbatch ./' scriptNames{sim}]);
        end
    end
end
