function [grid, W] = buildLatticeGrid(lat_props,V_U,V_L,T_amb)
    % BUILDLATTICEGRID Builds a grid of lattice_elements for the model
    % Accepts a struct with lattice properties, upper/lower applied voltage
    % at t=0 and ambient temperature. Returns lattice_elements array and
    % list of vertices of tetrahedral elements.

    debugDisp('Building lattice...');
    
    global PROPS;
    q = 1.602e-19;                % [C] Elemental charge
    
    L_step = PROPS.L_step*1e-10;  % [m] Lattice spacing step

    E_C_el   = PROPS.E_C_el*q;    % [J] Electrode conduction band energy
    E_C_il   = PROPS.E_C_il*q;    % [J] IL conduction band energy
    E_Tmu    = PROPS.E_Tmu*q;     % [J] Average trap energy
    E_Tdelta = PROPS.E_Tdelta*q;  % [J] Trap energy deviation
    R_T      = PROPS.R_T;         % [m] Trap capture radius
    epsilon  = PROPS.epsilon;     % [-] Relative dielectric permittivity of HfO2
    
    N_x      = lat_props.N_x;     % [-] Points in X direction
    N_y      = lat_props.N_y;     % [-] Points in Y direction
    N_z      = lat_props.N_z;     % [-] Points in Z direction
    
    % Grid size as a vector
    gs = [N_x N_y N_z];
    
    % Translate total vacancies/cm3 to local vacancy generation probability
    vacancyDistribution = lat_props.vacancyDistribution*((L_step^3)/1e-6);
    GBDistribution = lat_props.GBDistribution*((L_step^3)/1e-6);
    
    grid_type = lat_props.grid_type;
    
    % Retrieve roughness information
    upperRoughness = randi([0 lat_props.electrodeRoughness],[N_x N_y]);
    lowerRoughness = randi([0 lat_props.electrodeRoughness],[N_x N_y]);
    
    % Retrieve impurity information
    impurityType = lat_props.impurityType;
    impurityWidth = lat_props.impurityWidth;
    impurityHeight = lat_props.impurityHeight;
    impurityPos = lat_props.impurityPos;
    
    % Preallocate grid
    debugDisp('Preallocating grid...');
    grid(1:N_x,1:N_y,1:N_z) = lattice_element();
    
    for ii = 1:N_x
        debugDisp(['  Slice ' num2str(ii) '/' num2str(N_x) '...']);
        for jj = 1:N_y
            for kk = 1:N_z
                gp = grid(ii,jj,kk);
                
                % Set basic properties of the lattice element
                gp.index = sub2ind(gs,ii,jj,kk);
                gp.x = ii*L_step;
                gp.y = jj*L_step;
                gp.z = kk*L_step;
                gp.T = T_amb;
                gp.epsilon = epsilon;
                
                % Set difference matrices
                A_row = [gp.index 0];
                if ii ~= 1;   A_row(1,2) = A_row(1,2)+1; A_row = [A_row; sub2ind(gs,ii-1,jj,kk) -1]; end
                if jj ~= 1;   A_row(1,2) = A_row(1,2)+1; A_row = [A_row; sub2ind(gs,ii,jj-1,kk) -1]; end
                if kk ~= 1;   A_row(1,2) = A_row(1,2)+1; A_row = [A_row; sub2ind(gs,ii,jj,kk-1) -1]; end
                if ii ~= N_x; A_row(1,2) = A_row(1,2)+1; A_row = [A_row; sub2ind(gs,ii+1,jj,kk) -1]; end
                if jj ~= N_y; A_row(1,2) = A_row(1,2)+1; A_row = [A_row; sub2ind(gs,ii,jj+1,kk) -1]; end
                if kk ~= N_z; A_row(1,2) = A_row(1,2)+1; A_row = [A_row; sub2ind(gs,ii,jj,kk+1) -1]; end
                A_row(:,2:end) = A_row(:,2:end)*L_step;
                gp.A_row = A_row;
                
                Nabla_row = zeros(0,4);
                Nabla_rowSelf = [gp.index 0 0 0];
                
                if ii ~= 1 && ii ~= N_x
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii-1,jj,kk) -0.5 0 0]; [sub2ind(gs,ii+1,jj,kk) 0.5 0 0]];
                elseif ii == 1
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii+1,jj,kk) 0.5 0 0]];
                    Nabla_rowSelf(2) = -0.5;
                elseif ii == N_x
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii-1,jj,kk) -0.5 0 0]];
                    Nabla_rowSelf(2) = 0.5;
                end
                
                if jj ~= 1 && jj ~= N_y
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii,jj-1,kk) 0 -0.5 0]; [sub2ind(gs,ii,jj+1,kk) 0 0.5 0]];
                elseif jj == 1
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii,jj+1,kk) 0 0.5 0]];
                    Nabla_rowSelf(3) = -0.5;
                elseif jj == N_y
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii,jj-1,kk) 0 -0.5 0]];
                    Nabla_rowSelf(3) = 0.5;
                end
                
                if kk ~= 1 && kk ~= N_z
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii,jj,kk-1) 0 0 -0.5]; [sub2ind(gs,ii,jj,kk+1) 0 0 0.5]];
                elseif kk == 1
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii,jj,kk+1) 0 0 0.5]];
                    Nabla_rowSelf(4) = -0.5;
                elseif kk == N_z
                    Nabla_row = [Nabla_row; [sub2ind(gs,ii,jj,kk-1) 0 0 -0.5]];
                    Nabla_rowSelf(4) = 0.5;
                end
                
                if any(Nabla_rowSelf(2:4)); Nabla_row = [Nabla_rowSelf; Nabla_row]; end
                Nabla_row(:,2:end) = Nabla_row(:,2:end)/L_step;
                gp.Nabla_row = Nabla_row;
                
                isLower = (kk <= (2 + lowerRoughness(ii,jj)));
                isUpper = (kk > N_z-(3 + upperRoughness(ii,jj)));
                isAir = false;
                
                % Inject an impurity
                if any(strcmp(impurityType,{'upper','lower','air','oxide'}))
                    if (((gp.x - impurityPos(1))^2 + (gp.y - impurityPos(2))^2)/impurityWidth^2 ...
                      + (gp.z - impurityPos(3))^2/impurityHeight^2) < 1
                        isLower = strcmp(impurityType,'lower');
                        isUpper = strcmp(impurityType,'upper');
                        isAir = strcmp(impurityType,'air');
                    end
                end
                if isLower
                    % Point is lower electrode
                    gp.type = 'lower'; % lower electrode
                    gp.phi = V_L;
                    gp.phi_ex = V_L;
                    gp.E = E_C_el; % electrode conduction band energy
                elseif isUpper
                    % Point is upper electrode
                    gp.type = 'upper'; % upper electrode
                    gp.phi = V_U;
                    gp.phi_ex = V_U;
                    gp.E = E_C_el; % electrode conduction band energy
                elseif isAir
                    gp.type = 'air';
                else
                % Determine if point is a vacancy
                    switch (grid_type)
                    case 'trap column'
                        % Column of traps in the middle of oxide
                        d = (ii - N_x/2)^2 + (jj - N_y/2)^2;
                        vacancyCondition = rand < 1-0.15*d;
                    case 'single trap'
                        % Single trap in the middle of oxide
                        vacancyCondition = (ii == round(N_x/2)) ...
                                        && (jj == round(N_y/2)) ...
                                        && (kk == round(N_z/2));
                    case 'trap line'
                        % Line of traps in the middle of oxide
                        vacancyCondition = (ii == round(N_x/2)) ... 
                                        && (jj == round(N_y/2));
                    case 'GB'
                        % Grain boundary in the middle of oxide
                        vacancyCondition = any(all(gbVacancyPositions == [ii jj kk],2));
                    case 'regular+GB'
                        % Grain boundary in middle with scattered vacancies
                        d = (ii - N_x/2)^2 + (jj - N_y/2)^2;
                        if d < 16
                            vacancyCondition = rand < GBDistribution;
                        else
                            vacancyCondition = rand < vacancyDistribution;
                        end
                    case 'filament'
                        % Filament in middle with scattered vacancies
                        d = (ii - N_x/2)^2 + (jj - N_y/2)^2;
                        w = 0.04*(kk-N_z/2)^2 + 4;
                        if sqrt(d) < w
                            vacancyCondition = rand < (1-0.6*d/w);
                        else
                            vacancyCondition = rand < vacancyDistribution;
                        end
                    otherwise
                        vacancyCondition = rand < vacancyDistribution;
                    end
                    if vacancyCondition
                        gp.type = 'V+2'; % vacancy
                        gp.E = E_Tmu + (2*rand-1)*E_Tdelta; % trap energy
                        gp.R_T = R_T; % trap capture radius
                    end
                end
                
                % Set grid point
                grid(ii,jj,kk) = gp;
            end
        end
    end
    
    % Find the closest lower and upper electrode and interface layers
    debugDisp('Setting closest electrode points...');
    
    electrodes = grid(grid.is({'upper','lower'}));
    
    for n = 1:numel(electrodes)
        ep = electrodes(n);
        % If the electrode point borders an oxide or V+2 point
        if any(grid(ep.A_row(:,1)).is('oxide') ...
             | grid(ep.A_row(:,1)).is('V+2'))
            % Set as interface layer
            grid(ep.index).isIL = true;
            if strcmp(ep.type,'upper')
                % If upper electrode, set type to OEL
                grid(ep.index).type = 'OEL';
            end
        end
    end
    
%     lowerElectrode = grid(grid.is('lower') & [grid.isIL]);
%     upperElectrode = grid(grid.is('OEL'));
%     
%     dgrid = grid(grid.is({'V+2'}) | [grid.isIL]);
%     
%     progressBarStep = numel(dgrid)/20;
%     
%     for n = 1:numel(dgrid)
%         if mod(n,progressBarStep) == 0
%             fprintf('.');
%         end
%         gp = dgrid(n);
%         
%         d_min = Inf;
%         i_min = 0;
%         for nn = 1:numel(lowerElectrode)
%             ep = lowerElectrode(nn);
%             d = sqrt((ep.x - gp.x)^2 + (ep.y - gp.y)^2 + (ep.z - gp.z)^2);
%             if d < d_min; d_min = d; i_min = ep.index; end
%         end
%         
%         gp.d_L = d_min;
%         gp.index_L = i_min;
%         
%         d_min = Inf;
%         i_min = 0;
%         for nn = 1:numel(upperElectrode)
%             ep = upperElectrode(nn);
%             d = sqrt((ep.x - gp.x)^2 + (ep.y - gp.y)^2 + (ep.z - gp.z)^2);
%             if d < d_min; d_min = d; i_min = ep.index; end
%         end
%         
%         gp.d_U = d_min;
%         gp.index_U = i_min;
%         
%         grid(gp.index) = gp;
%     end
    
    % Get the z index of the upper and lower interface layers
    kk_ILu = N_z-2;
    kk_ILl = 2;

    for ii = 1:N_x
        for jj = 1:N_y
            % Set the upper/lower interface layers
            grid(ii,jj,kk_ILu).isIL = true;
            grid(ii,jj,kk_ILl).isIL = true;
            grid(ii,jj,kk_ILu).E = E_C_il;
            grid(ii,jj,kk_ILl).E = E_C_il;

            uE = grid(ii,jj,kk_ILu);
            lE = grid(ii,jj,kk_ILl);

            % Set the closest upper/lower interface indices
            for kk = 1:N_z
                gp = grid(ii,jj,kk);

                gp.index_U = uE.index;
                gp.index_L = lE.index;
                gp.d_U = uE.z - gp.z;
                gp.d_L = gp.z - lE.z;

                grid(ii,jj,kk) = gp;
            end
        end
    end
    
    % Warn if there are no electrodes
    if ~any(find(strcmp({grid.type},'upper')))
        warning('No upper electrode points detected.')
    end
    if ~any(find(strcmp({grid.type},'lower')))
        warning('No lower electrode points detected.')
    end

    % Flatten grid
    grid = grid(:);
    
    debugDisp('Build Lattice Grid done.');
end

function debugDisp(text)
    if checkIfDebugging('buildLatticeGrid')
        disp(text);
    end
end