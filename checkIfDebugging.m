function check = checkIfDebugging(f)
    global DEBUG_MODE;
    if isempty(DEBUG_MODE); DEBUG_MODE.on = false; end
    check = DEBUG_MODE.on && (any(strcmp(DEBUG_MODE.do,'all') | strcmp(DEBUG_MODE.do,f)));
end

